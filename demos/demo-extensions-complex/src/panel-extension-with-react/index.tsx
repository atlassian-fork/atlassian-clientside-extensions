import React, { useEffect } from 'react';
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';

type ComponentContext = { value: number };

type MyComponentProps = PanelExtension.MountProps & {
    context: ComponentContext;
};

function MyComponent({ context }: MyComponentProps) {
    const { value } = context;

    useEffect(() => {
        function eventListener() {
            // eslint-disable-next-line no-console
            console.log(`Event Listener from panel-extension-with-react for val ${context.value}`);
        }

        document.addEventListener('my-event', eventListener);

        return function cleanUp() {
            document.removeEventListener('my-event', eventListener);
        };
    });

    return (
        <div>
            <h4>Panel Extension with React</h4>
            <p>
                Extension: <b>web-panel-react</b>
            </p>
            <p>context.value: {value}</p>
            <p>
                {/* eslint-disable-next-line no-alert */}
                <button data-testid="panel-plugin-with-react-hi-button" type="button" onClick={() => alert('Hello from a React Panel! 😄')}>
                    Say Hi!
                </button>
                {/* eslint-disable-next-line no-alert */}
                <button
                    data-testid="panel-plugin-with-react-bye-button"
                    type="button"
                    onClick={() => alert('Bye bye from a React Panel! 👋')}
                >
                    Say Bye!
                </button>
            </p>
        </div>
    );
}

/**
 * @clientside-extension
 *
 * @extension-point reff.plugins-example-location
 */
export default PanelExtension.factory((api, context: ComponentContext) => {
    return {
        label: 'React Panel',
        onAction(panelApi: PanelExtension.Api) {
            renderElementAsReact<MyComponentProps>(panelApi, MyComponent, { context });
        },
    };
});
