import { ButtonExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point reff.plugins-example-location
 */
export default ButtonExtension.factory(api => {
    const getLabel = (prev?: string) => {
        const randomWord = btoa(Math.random().toString());
        const previousWord = prev ? prev.split('Current word: ')[1].trim() : '(not set)';
        return `Generate words. Previous word: ${previousWord}. Current word: ${randomWord}`;
    };

    return {
        label: getLabel(),
        onAction() {
            api.updateAttributes(prev => ({
                label: getLabel(prev.label as string),
            }));
        },
    };
});
