import { ButtonExtension } from '@atlassian/clientside-extensions';
import { StatefulExtensionPointData } from '@atlassian/clientside-extensions-demo-product/src/pages/03-stateful/types';

let clickCount = 0;

/**
 * @clientside-extension
 * @extension-point demo.03-stateful
 */
export default ButtonExtension.factory((api, providedData: StatefulExtensionPointData) => {
    return {
        label: `Click this button ${providedData.randomNumber} times, please.`,
        onAction: () => alert(`Clicks so far: ${++clickCount}. Keep it up!`),
    };
});
