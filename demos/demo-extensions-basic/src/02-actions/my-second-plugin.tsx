import { ButtonExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point demo.02-actions
 */
export default ButtonExtension.factory(() => {
    return {
        label: 'No, click this one!',
        onAction: () => alert('you did it again!'),
    };
});
