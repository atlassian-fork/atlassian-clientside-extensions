import { ButtonExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point demo.02-actions
 */
export default ButtonExtension.factory(() => {
    return {
        label: 'Click this button',
        onAction: () => alert('You did it!'),
    };
});
