import { ButtonExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point demo.01-basic
 */
export default ButtonExtension.factory(() => {
    return {
        label: 'A simple button',
    };
});
