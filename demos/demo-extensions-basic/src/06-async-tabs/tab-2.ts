import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point demo.06-tabs
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: 'Async Tab 2',
        onAction() {
            return import(/* webpackChunkName: "tab-2-content" */ './tab-2-content');
        },
    };
});
