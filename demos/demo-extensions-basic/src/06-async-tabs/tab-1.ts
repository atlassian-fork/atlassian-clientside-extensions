import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point demo.06-tabs
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: 'Async Tab 1',
        onAction() {
            return import(/* webpackChunkName: "tab-1-content" */ './tab-1-content');
        },
        dataProvider() {
            return {
                greetings: [
                    'Why, hello there!',
                    'Sure is nice to see ya!',
                    `You're always welcome here!`,
                    'Howdy, partner!',
                    `Hey hey hey!`,
                    'Yo.',
                ],
            };
        },
    };
});
