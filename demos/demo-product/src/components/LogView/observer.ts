import { LoggerPayload, LogLevel, observeLogger } from '@atlassian/clientside-extensions-debug';
import { useMemo, useState } from 'react';

const componentsToReadable = (components?: string | string[]) => {
    if (!components) {
        return '';
    }

    const componentsArray = Array.isArray(components) ? components : components.split('.');

    return componentsArray.join(' → ');
};

const metaFormatter = (_: unknown, value: unknown): string => {
    if (value === null) {
        return 'null';
    }

    if (value === undefined) {
        return 'undefined';
    }

    if (Array.isArray(value)) {
        return value.join(', ');
    }

    if (typeof value === 'function') {
        return value.toString();
    }

    return value as string;
};
const metaValueToReadable = (value: unknown) => {
    return JSON.stringify(value, metaFormatter)
        .replace(/^"|"$/g, '')
        .replace(/"/g, ' ');
};
const metaToReadable = (meta?: LoggerPayload['meta']) => {
    if (!meta) {
        return {};
    }

    return Object.keys(meta).reduce((agg, key) => {
        agg[key] = metaValueToReadable(meta[key]);
        return agg;
    }, {} as { [key: string]: string });
};

export type LogPayload = {
    uniqueKey: string;
    time: string;
    component: string;
    message: string;
    meta: { [key: string]: string };
};
// eslint-disable-next-line import/prefer-default-export
export const useLogger = (levels: LogLevel[]) => {
    const [logs, addLogs] = useState<LogPayload[]>([]);
    useMemo(() => {
        observeLogger(payload => {
            if (!levels.includes(payload.level)) {
                return;
            }
            addLogs(
                old =>
                    [
                        ...old,
                        {
                            uniqueKey: `${+new Date()}${Math.random()}`,
                            time: new Date().toLocaleTimeString(),
                            component: componentsToReadable(payload.components),
                            message: payload.message,
                            meta: metaToReadable(payload.meta),
                        },
                    ] as LogPayload[],
            );
        });
    }, [...levels]);

    return logs;
};
