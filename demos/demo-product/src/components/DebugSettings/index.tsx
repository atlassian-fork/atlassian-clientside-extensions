import React from 'react';
import { useDebug, useLogging, useValidation, useDiscovery } from '@atlassian/clientside-extensions-components';

interface FlagComponentProps {
    flag: string;
    isFlagEnabled: boolean;
    setFlagEnabled: (value: boolean) => void;
}

const FlagComponent: React.FunctionComponent<FlagComponentProps> = ({ flag, isFlagEnabled, setFlagEnabled }) => {
    return (
        <p key={flag}>
            <label key={flag} htmlFor={flag}>
                <input
                    type="checkbox"
                    id={flag}
                    checked={isFlagEnabled}
                    onChange={() => {
                        setFlagEnabled(!isFlagEnabled);
                    }}
                />
                {flag}
            </label>
        </p>
    );
};

const DebugSettingsHook: React.FunctionComponent = () => {
    const [isDebugEnabled, setDebugEnabled] = useDebug();
    const [isLoggingEnabled, setLoggingEnabled] = useLogging();
    const [isValidationEnabled, setValidationEnabled] = useValidation();
    const [isDiscoveryEnabled, setDiscoveryEnabled] = useDiscovery();
    return (
        <>
            <FlagComponent flag="Debug" isFlagEnabled={isDebugEnabled} setFlagEnabled={setDebugEnabled} />
            <FlagComponent flag="Logging" isFlagEnabled={isLoggingEnabled} setFlagEnabled={setLoggingEnabled} />
            <FlagComponent flag="Validation" isFlagEnabled={isValidationEnabled} setFlagEnabled={setValidationEnabled} />
            <FlagComponent flag="Discovery" isFlagEnabled={isDiscoveryEnabled} setFlagEnabled={setDiscoveryEnabled} />
        </>
    );
};

export default DebugSettingsHook;
