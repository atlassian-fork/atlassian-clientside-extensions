import React, { useEffect, useState } from 'react';

interface Props {
    glyph: string;
    label: string;
}

function AsyncIcon({ glyph, label }: Props) {
    const [IconComponent, setIconComponent] = useState(null);

    function importIconComponent(name: string) {
        if (!name) {
            return;
        }

        // eslint-disable-next-line
        import('@atlaskit/icon/glyph/' + name + '.js')
            .then(icon => {
                setIconComponent(icon.default);
            })
            .catch(() => {
                console.warn(`Can't find icon with name: ${name}`);

                setIconComponent(null);
            });
    }

    useEffect(() => {
        importIconComponent(glyph);
    }, [glyph]);

    if (IconComponent) {
        return React.cloneElement(IconComponent, { label });
    }

    return null;
}

export default AsyncIcon;
