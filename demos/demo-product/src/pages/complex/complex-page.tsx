/* eslint-disable react/jsx-props-no-spreading */
import React, { FunctionComponent, useState } from 'react';
import {
    ButtonHandler,
    ExtensionPoint,
    ExtensionPointInfo,
    LinkHandler,
    ModalHandler,
    PanelHandler,
    useExtensionsAll,
} from '@atlassian/clientside-extensions-components';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import Button from '@atlaskit/button';

import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';
import { schema, contextSchema } from './schema';
import DebugSettings from '../../components/DebugSettings';
import LogView from '../../components/LogView';

const headerStyles = {
    marginTop: '15px',
};

const listStyles = {
    listStyle: 'none',
};

const listItemStyles = {
    margin: '12px 0',
    padding: '12px 8px',
};

type ExtensionProps = { extension: PluginDescriptor; collapsible?: boolean };

const ModalExtensionRenderer: FunctionComponent<ExtensionProps> = ({ extension }) => {
    const [isOpen, setIsOpen] = useState(false);

    const { onAction, ...renderableAttributes } = extension.attributes;
    return (
        <li key={extension.key} style={listItemStyles}>
            <Button type="button" {...renderableAttributes} onClick={() => setIsOpen(true)}>
                {renderableAttributes.label}
            </Button>
            <ModalHandler.ModalRenderer
                isOpen={isOpen}
                onClose={() => setIsOpen(false)}
                render={onAction as ModalHandler.ModalRenderExtension}
            />
        </li>
    );
};

const CollapsiblePanelExtensionRenderer: FunctionComponent<ExtensionProps> = ({ extension }) => {
    const [isActive, setIsActive] = useState(false);
    const { onAction, ...renderableAttributes } = extension.attributes;
    return (
        <li key={extension.key} style={listItemStyles}>
            <Button type="button" {...renderableAttributes} onClick={() => setIsActive(!isActive)}>
                {renderableAttributes.label}
            </Button>

            {isActive && <PanelHandler.PanelRenderer render={onAction as PanelHandler.PanelRenderExtension} />}
        </li>
    );
};

const PanelExtensionRenderer: FunctionComponent<ExtensionProps> = ({ extension }) => {
    const { onAction } = extension.attributes;
    return (
        <li key={extension.key} style={listItemStyles}>
            <PanelHandler.PanelRenderer render={onAction as PanelHandler.PanelRenderExtension} />
        </li>
    );
};

const ButtonExtensionRenderer: FunctionComponent<ExtensionProps> = ({ extension }) => {
    const { onAction, ...renderableAttributes } = extension.attributes;
    return (
        <li key={extension.key} style={listItemStyles}>
            <Button type="button" {...renderableAttributes} onClick={onAction as () => void}>
                {renderableAttributes.label}
            </Button>
            <ButtonHandler.ButtonRenderer extension={extension} />
        </li>
    );
};

const LinkExtensionRenderer: FunctionComponent<ExtensionProps> = ({ extension }) => {
    const { url, ...renderableAttributes } = extension.attributes;
    return (
        <li key={extension.key} style={listItemStyles}>
            <a href={url as string} {...renderableAttributes}>
                {renderableAttributes.label}
            </a>
            <LinkHandler.LinkRenderer extension={extension} />
        </li>
    );
};

const ExtensionRenderer = ({ type, extension, collapsible }: { type: string } & ExtensionProps) => {
    if (type === 'modal') {
        return <ModalExtensionRenderer extension={extension} />;
    }

    if (type === 'button') {
        return <ButtonExtensionRenderer extension={extension} />;
    }

    if (type === 'link') {
        return <LinkExtensionRenderer extension={extension} />;
    }

    if (type === 'panel') {
        return collapsible ? <CollapsiblePanelExtensionRenderer extension={extension} /> : <PanelExtensionRenderer extension={extension} />;
    }

    return null;
};

function ExtensionPointWithHook({ context, collapsible }: { context: { value: number }; collapsible: boolean }) {
    const [extensionDescriptors, , isLoading] = useExtensionsAll('reff.plugins-example-location', context, { schema, contextSchema });

    return (
        <>
            <h2 style={headerStyles}>
                {isLoading && <div>loading resources...</div>}
                Extension point provided by useExtensions hooks
                <ExtensionPointInfo name="reff.plugins-example-location" schemas={{ schema, contextSchema }} />
            </h2>

            <ul style={listStyles}>
                {extensionDescriptors.map(extension => (
                    <ExtensionRenderer
                        key={extension.key}
                        type={extension.attributes.type as string}
                        extension={extension}
                        collapsible={collapsible}
                    />
                ))}
            </ul>
        </>
    );
}

const ComplexPage = () => {
    const [value, setValue] = useState(0);
    const [isExtensionPointHidden, setIsExtensionPointHidden] = useState(false);
    const [isUsingCollapsiblePanels, setUsingCollapsiblePanels] = useState(false);

    return (
        <>
            <section>
                <h2 style={headerStyles} data-testid="current-value">
                    Context value: {value}
                </h2>
                <button type="button" onClick={() => setValue(value - 1)} data-testid="decrement-current-value">
                    -1
                </button>
                <button type="button" onClick={() => setValue(value + 1)} data-testid="increment-current-value">
                    +1
                </button>
            </section>

            <section>
                <h2 style={headerStyles}>Show/Hide extension points (testing re-rendering)</h2>
                <button type="button" onClick={() => setIsExtensionPointHidden(!isExtensionPointHidden)} data-testid="show-hide-location">
                    {isExtensionPointHidden ? 'Show' : 'Hide'}
                </button>
            </section>

            <section>
                <h2 style={headerStyles}>Use collapsible panels</h2>
                <button type="button" onClick={() => setUsingCollapsiblePanels(!isUsingCollapsiblePanels)}>
                    {isUsingCollapsiblePanels ? 'Yes' : 'No'}
                </button>
            </section>

            <section>
                <h2 style={headerStyles}>Show logging overlay</h2>
                <LogView />
            </section>

            <section>
                <h2 style={headerStyles}>Debug utilities:</h2>
                <DebugSettings />
            </section>

            {isExtensionPointHidden ? null : (
                <>
                    <div data-testid="location-fragment">
                        {/* hook implementation */}
                        <ExtensionPointWithHook context={{ value }} collapsible={isUsingCollapsiblePanels} />

                        {/* component implementation */}
                        <h2 style={headerStyles}>
                            Extension point provided by ExtensionPoint component
                            <ExtensionPointInfo name="reff.old-web-items-location" schemas={{ schema, contextSchema }} />
                        </h2>
                        <ExtensionPoint name="reff.old-web-items-location" context={{ value }} options={{ schema, contextSchema }}>
                            {(extensionDescriptors, unsupportedExtensionDescriptors) => (
                                <ul style={listStyles}>
                                    {extensionDescriptors.map(extension => (
                                        <ExtensionRenderer
                                            key={extension.key}
                                            type={extension.attributes.type as string}
                                            extension={extension}
                                        />
                                    ))}
                                    {unsupportedExtensionDescriptors.map(extension => (
                                        <ExtensionRenderer key={extension.key} type="link" extension={extension} />
                                    ))}
                                </ul>
                            )}
                        </ExtensionPoint>
                    </div>
                </>
            )}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, ComplexPage);
};
