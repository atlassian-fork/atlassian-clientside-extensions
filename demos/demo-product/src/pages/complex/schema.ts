export const schema = {
    properties: {
        type: {
            type: 'string',
            description: 'Web-item type',
            enum: ['modal', 'link', 'button', 'panel'],
        },
        glyph: {
            type: 'string',
            description: 'Glyph name',
            enum: ['cross', 'check'],
        },
        tooltip: {
            type: 'string',
            description: 'Tooltip content',
        },
        hidden: {
            type: 'boolean',
            description: 'Hidden flag to hide web-item',
        },
        onAction: {
            description: 'Callback triggered on user interaction with the Web-item. Signature depends on web-item type.',
            typeof: 'function',
        },
    },
    required: ['label'],
    type: 'object',
};

export const contextSchema = {
    type: 'object',
    properties: {
        value: {
            type: 'number',
        },
    },
};
