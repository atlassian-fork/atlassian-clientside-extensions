import React from 'react';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import { useExtensions } from '@atlassian/clientside-extensions-components';
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';

/**
 * Defines the attributes plugin developers are allowed to provide to this extension point.
 * Anything not on the list will not be given to the product. Any attributes that don't
 * match the appropriate data type will also not be provided.
 */
const schema = {
    type: 'object',
    properties: {
        type: {
            type: 'string',
            enum: ['button'],
        },
        label: {
            type: 'string',
            description: 'Text to render on the button',
        },
    },
};

function asButtons(extension: PluginDescriptor) {
    console.log(extension.attributes);
    return (
        <button key={extension.key} type="button">
            {extension.attributes.label || 'Default button text'}
        </button>
    );
}

const BasicDemo = () => {
    const extensionDescriptors = useExtensions('demo.01-basic', null, { schema });

    return (
        <>
            <h1>Basic demo</h1>
            <p>We create a simple extension point, then render everything registered in it as a button.</p>

            {extensionDescriptors.map(asButtons)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, BasicDemo);
};
