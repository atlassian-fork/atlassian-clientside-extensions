export const schema = {
    type: 'object',
    properties: {
        type: {
            type: 'string',
            enum: ['panel'],
        },
        label: {
            type: 'string',
            description: 'Text that will appear in the tab.',
        },
        onAction: {
            typeof: 'function',
            description: 'Callback to allow the plugin to define content rendered inside the tab panel.',
        },
    },
};

export const contextSchema = {
    type: 'object',
    properties: {
        title: {
            type: 'string',
        },
    },
};
