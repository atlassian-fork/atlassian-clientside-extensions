import React, { useState } from 'react';
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';
import AuiTabsDemo from './using-aui';
import AkTabsDemo from './using-ak';
import { ExtensionPointDataDemo05 } from './types';

const pluginData: ExtensionPointDataDemo05 = {
    title: `Dragon's rest`,
};

const impls: Record<string, (data: ExtensionPointDataDemo05) => JSX.Element> = {
    aui: data => <AuiTabsDemo data={data} />,
    atlaskit: data => <AkTabsDemo data={data} />,
};

const TabsDemo = () => {
    const [impl] = useState('atlaskit');

    return (
        <>
            <h1>Building tabs</h1>
            <p>
                Product developers can combine <dfn>panel</dfn> plugin types with a rich schema to render complex inter-related components,
                such as tabs.
            </p>

            {impls[impl](pluginData)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, TabsDemo);
};
