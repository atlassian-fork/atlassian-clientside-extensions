import React, { FC } from 'react';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import { PanelHandler, useExtensionsAll } from '@atlassian/clientside-extensions-components';
import { contextSchema, schema } from './schema';
// eslint-disable-next-line
import 'wr-dependency!com.atlassian.auiplugin:tabs';
import { ExtensionPointDataDemo05 } from './types';

const asTabItems = (ext: PluginDescriptor) => {
    const id = ext.key;
    const { label, isActive } = ext.attributes;
    return (
        <li className={`menu-item ${isActive ? 'active-tab' : ''}`} key={id}>
            <a href={`#${id}`}>{label}</a>
        </li>
    );
};

const asTabPanes = (ext: PluginDescriptor) => {
    const id = ext.key;
    const { label, onAction, isActive } = ext.attributes;
    return (
        <div className={`tabs-pane ${isActive ? 'active-pane' : ''}`} id={id} key={id}>
            <h2>Welcome to {label}</h2>
            <PanelHandler.PanelRenderer render={onAction as PanelHandler.PanelRenderExtension} />
        </div>
    );
};

const AUITabsDemo: FC<{ data: ExtensionPointDataDemo05 }> = ({ data }) => {
    const [descriptors, , isLoading] = useExtensionsAll('demo.05-tabs', data, { schema, contextSchema });

    // Initialise tabs once all plugins have loaded.
    if (!isLoading && descriptors.length) {
        // select the first tab by default.
        descriptors[0].attributes.isActive = true;

        // @ts-ignore
        // eslint-disable-next-line no-undef
        AJS.tabs.setup();
    }

    return (
        <section className="aui-tabs horizontal-tabs">
            <ul className="tabs-menu">{descriptors.map(asTabItems)}</ul>
            {descriptors.map(asTabPanes)}
        </section>
    );
};

export default AUITabsDemo;
