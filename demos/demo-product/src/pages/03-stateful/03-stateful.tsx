import React, { useState, useEffect } from 'react';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import { useExtensions } from '@atlassian/clientside-extensions-components';
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';
import { StatefulExtensionPointData } from './types';

const schema = {
    type: 'object',
    properties: {
        type: {
            type: 'string',
            enum: ['button'],
        },
        label: {
            type: 'string',
            description: 'Text to render on the button',
        },
        onAction: {
            typeof: 'function',
            description: 'Callback will be run when user clicks the button',
        },
    },
};

const contextSchema = {
    type: 'object',
    properties: {
        foo: {
            type: 'string',
        },
        randomNumber: {
            type: 'number',
        },
    },
};

function asButtons(extension: PluginDescriptor) {
    const { label, onAction } = extension.attributes;
    return (
        <button key={extension.key} type="button" onClick={onAction as () => {}}>
            {label}
        </button>
    );
}

const baseData: StatefulExtensionPointData = {
    foo: 'bar',
    randomNumber: 7,
};

const StatefulDemo = () => {
    const [extensionData, setExtensionData] = useState(baseData);
    const extensionDescriptors = useExtensions('demo.03-stateful', extensionData, { schema, contextSchema });

    useEffect(() => {
        const interval = setInterval(() => {
            const randomNumber = Math.round(Math.random() * 1000);
            setExtensionData({ ...extensionData, randomNumber });
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    return (
        <>
            <h1>Stateful extensions</h1>
            <p>Extensions can react to context provided via the plugin point. They can also maintain their own state.</p>

            {extensionDescriptors.map(asButtons)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, StatefulDemo);
};
