import React, { FC } from 'react';

import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import { AsyncPanelHandler, useExtensions } from '@atlassian/clientside-extensions-components';
import Tabs from '@atlaskit/tabs';
import { TabData, TabContentComponentProvided } from '@atlaskit/tabs/types';
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';

const schema = {
    type: 'object',
    properties: {
        type: {
            type: 'string',
            enum: ['async-panel'],
        },
        label: {
            type: 'string',
            description: 'Text that will appear in the tab.',
        },
        onAction: {
            typeof: 'function',
            description: 'Callback to allow the plugin to define content rendered inside the tab panel.',
        },
        dataProvider: {
            typeof: 'function',
            description: 'Callback that returns payload that gets passed to the dynamically imported panel handler specified in `onAction`',
        },
    },
};

const CustomContent = (tabData: TabContentComponentProvided) => {
    if (!tabData.data) {
        return null;
    }

    return (
        <AsyncPanelHandler.AsyncPanelRenderer
            location="demo.06-tabs"
            pluginKey={tabData.data.key}
            renderProvider={tabData.data.onAction as AsyncPanelHandler.AsyncPanelRenderExtension}
            data={tabData.data.dataProvider()}
            fallback={<div>IM LOADING</div>}
        />
    );
};

const asTabParams = (ext: PluginDescriptor): TabData => {
    const id = ext.key;
    const { label, onAction, dataProvider = () => {} } = ext.attributes;
    return {
        label: `${label}`,
        key: id,
        onAction,
        dataProvider,
    };
};

const AsyncTabsDemo: FC = () => {
    const descriptors = useExtensions('demo.06-tabs', null, { schema });

    return (
        <section>
            <Tabs components={{ Content: CustomContent }} tabs={descriptors.map(asTabParams)} />
        </section>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, AsyncTabsDemo);
};
