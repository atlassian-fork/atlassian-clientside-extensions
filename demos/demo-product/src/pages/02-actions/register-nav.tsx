import { AsyncPanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default AsyncPanelExtension.factory(() => {
    return {
        label: 'Actions',
        section: 'basic.section',
        onAction() {
            return import(/* webpackChunkName: "demo-actions" */ './02-actions');
        },
    };
});
