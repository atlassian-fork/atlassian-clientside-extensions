import React from 'react';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import { useExtensions } from '@atlassian/clientside-extensions-components';
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';

/**
 * Schema attributes can be given different data types, such as 'string' and 'function'.
 * Valid types are listed in the "JSON Schema" spec.
 * https://json-schema.org/
 */
const schema = {
    type: 'object',
    properties: {
        type: {
            type: 'string',
            enum: ['button'],
        },
        label: {
            type: 'string',
            description: 'Text to render on the button',
        },
        onAction: {
            typeof: 'function',
            description: 'Callback will be run when user clicks the button',
        },
    },
};

function asButtons(extension: PluginDescriptor) {
    const { label, onAction } = extension.attributes; // baz
    return (
        <button key={extension.key} type="button" onClick={onAction as () => {}}>
            {label}
        </button>
    );
}

const ActionsDemo = () => {
    const extensionDescriptors = useExtensions('demo.02-actions', null, { schema });

    return (
        <>
            <h1>Basic demo with actions</h1>
            <p>Extensions can provide whatever you let them in the schema, such as function callbacks.</p>

            {extensionDescriptors.map(asButtons)}
        </>
    );
};

export default (panelApi: PanelExtension.Api) => {
    renderElementAsReact(panelApi, ActionsDemo);
};
