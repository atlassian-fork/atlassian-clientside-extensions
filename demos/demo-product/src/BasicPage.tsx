import React from 'react';

const BasicPage: React.FC = ({ children }) => {
    return (
        <div className="aui-page-panel">
            <div className="aui-page-panel-content">
                <h1>Client-side Extensions</h1>
                {children}
            </div>
        </div>
    );
};

export default BasicPage;
