// eslint-disable-next-line import/no-unresolved,import/no-webpack-loader-syntax
import 'wr-dependency!com.atlassian.auiplugin:aui-sidebar';
import React, { FC, useMemo } from 'react';
import { render } from 'react-dom';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import { AsyncPanelHandler, useExtensions } from '@atlassian/clientside-extensions-components';
import { HashRouter as Router, NavLink, NavLinkProps, Redirect, Route, Switch, withRouter } from 'react-router-dom';
import kebabCase from 'lodash.kebabcase';
import BasicPage from './BasicPage';

const getPathFromLabel = (label: string) => {
    return `/${kebabCase(label)}`;
};

interface NavSectionDescriptor {
    section?: PluginDescriptor;
    items: PluginDescriptor[];
}

interface NavSectionDescriptorCollection {
    [key: string]: NavSectionDescriptor;
}

const NavLinkWithRouter = (withRouter(props => {
    const { location } = props;
    const { to, activeClassName, className, children } = (props as unknown) as NavLinkProps;
    const isActive = location.pathname === to;
    return (
        <li className={isActive ? activeClassName : undefined}>
            <NavLink to={to} className={className}>
                {children}
            </NavLink>
        </li>
    );
}) as unknown) as FC<NavLinkProps>;

const AuiSidebar: FC = ({ children }) => {
    return (
        <div className="aui-sidebar">
            <div className="aui-sidebar-wrapper">
                <div className="aui-sidebar-body">
                    <nav className="aui-navgroup aui-navgroup-vertical">
                        <div className="aui-navgroup-inner">{children}</div>
                    </nav>
                </div>
            </div>
        </div>
    );
};

interface AuiSidebarGroupProps {
    heading: string;
}
const AuiSidebarGroup: FC<AuiSidebarGroupProps> = ({ heading, children }) => {
    return (
        <div className="aui-sidebar-group">
            <div className="aui-nav-heading" title={heading}>
                <strong>{heading}</strong>
            </div>

            <ul className="aui-nav" title={heading}>
                {children}
            </ul>
        </div>
    );
};

const schema = {
    oneOf: [
        {
            type: 'object',
            properties: {
                type: {
                    const: 'section',
                },
                label: {
                    type: 'string',
                    description: 'Name of page in navigation',
                },
                section: {
                    type: 'string',
                    description: 'Id for the section this extension creates',
                },
                weight: {
                    type: 'number',
                    description: 'The weight defines the order of the sections - from lowest to highest number',
                },
            },
        },
        {
            type: 'object',
            properties: {
                type: {
                    const: 'async-panel',
                },
                label: {
                    type: 'string',
                    description: 'Name of page in navigation',
                },
                section: {
                    type: 'string',
                    description: 'Id of the section this link appears under',
                },
                onAction: {
                    typeof: 'function',
                    description: 'Callback that will render the page if navigated to',
                },
            },
        },
    ],
};

const MyApp = () => {
    const descriptors = useExtensions('demo-product.extend-navigation', null, { schema });
    const navigationItems = useMemo(() => descriptors.filter(descriptor => descriptor.attributes.type === 'async-panel'), [descriptors]);
    const navigationSections = useMemo(() => descriptors.filter(descriptor => descriptor.attributes.type === 'section'), [descriptors]);

    const extendedNavigationSections = useMemo(
        () =>
            Object.entries(
                navigationItems.reduce(
                    (agg, descriptor) => {
                        const section = descriptor.attributes.section as string;

                        if (!agg[section]) {
                            const navSection = navigationSections.find(descriptor => descriptor.attributes.section === section);
                            if (!navSection) {
                                // no such nav section available - add to "ungrouped"
                                agg.ungrouped.items.push(descriptor);
                                return agg;
                            }

                            agg[section] = {
                                section: navSection,
                                items: [descriptor],
                            };
                        } else {
                            agg[section].items.push(descriptor);
                        }

                        return agg;
                    },
                    { ungrouped: { items: [] } } as NavSectionDescriptorCollection,
                ),
            ).sort(([, valA], [, valB]) => {
                const weightA = (valA.section?.attributes?.weight as number) ?? Infinity;
                const weightB = (valB.section?.attributes?.weight as number) ?? Infinity;
                return weightA - weightB;
            }),
        [navigationItems, navigationSections],
    );

    return (
        <Router>
            <AuiSidebar>
                {extendedNavigationSections.map(([sectionId, navigationSection]) => {
                    const isUngrouped = sectionId === 'ungrouped';
                    const label = isUngrouped ? 'Other' : (navigationSection.section.attributes.label as string);
                    if (isUngrouped && navigationSection.items.length === 0) {
                        return null;
                    }
                    return (
                        <AuiSidebarGroup key={label} heading={label}>
                            {navigationSection.items.map(descriptor => {
                                const { key } = descriptor;
                                const { label } = descriptor.attributes;
                                return (
                                    <NavLinkWithRouter
                                        key={key}
                                        to={getPathFromLabel(label as string)}
                                        className="aui-nav-item"
                                        activeClassName="aui-nav-selected"
                                    >
                                        {label}
                                    </NavLinkWithRouter>
                                );
                            })}
                        </AuiSidebarGroup>
                    );
                })}
            </AuiSidebar>
            <BasicPage>
                <Switch>
                    {navigationItems.map(descriptor => {
                        const { key } = descriptor;
                        const { label, dataProvider, onAction } = descriptor.attributes;
                        const data = typeof dataProvider === 'function' ? dataProvider() : {};
                        return (
                            <Route key={key} path={getPathFromLabel(label as string)}>
                                <AsyncPanelHandler.AsyncPanelRenderer
                                    pluginKey={key}
                                    location="demo-product.extend-navigation"
                                    data={data}
                                    renderProvider={onAction as AsyncPanelHandler.AsyncPanelRenderExtension}
                                    fallback={<div>Loading Page....</div>}
                                />
                            </Route>
                        );
                    })}
                </Switch>
                <Route exact path="/">
                    {/* this one is a bit dodgy */}
                    <Redirect to={getPathFromLabel('Everything and the kitchen sink')} />
                </Route>
            </BasicPage>
        </Router>
    );
};

/** Boot our demos! */
const container = document.querySelector('#content');
render(<MyApp />, container);
