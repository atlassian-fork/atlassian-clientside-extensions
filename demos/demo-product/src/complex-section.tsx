import { SectionExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 *
 * @extension-point demo-product.extend-navigation
 */
export default SectionExtension.factory(() => {
    return {
        label: 'Complex examples',
        section: 'complex.examples.demo',
        weight: 1,
    };
});
