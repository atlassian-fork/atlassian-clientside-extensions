/* eslint-disable*/
(function() {
    function getPanelContent() {
        return `
            <h4>Cool panel</h4>
            <p>This is some cool content generated with JS</p>
        `;
    }

    function panelPluginFactory(pluginAPI) {
        function onClick() {
            alert('You clicked a button');
        }

        return {
            type: 'panel',
            label: 'Panel with JS',
            onAction: function onAction(panelAPI) {
                panelAPI
                    .onMount(function(container) {
                        var button = document.createElement('button');
                        button.setAttribute('data-testid', 'panel-plugin-button');
                        button.innerText = 'Click me!';
                        button.addEventListener('click', onClick);

                        container.innerHTML = getPanelContent();
                        container.appendChild(button);
                    })
                    .onUnmount(function(container) {
                        console.log('removed event listener for button');
                        // Don't forget to clean-up your code
                        var button = container.querySelector('button');
                        button.removeEventListener('click', onClick);
                    });
            },
        };
    }

    require(['@atlassian/clientside-extensions-registry'], function(registry) {
        registry.registerExtension('com.atlassian.plugins.atlassian-clientside-extensions-demo:panel-plugin-js', panelPluginFactory);
    });
})();
