/* eslint-disable*/
(function() {
    function buttonPluginFactory(pluginAPI) {
        var timesClicked = 0;

        function getLabel() {
            return 'Button clicked ' + timesClicked + ' times';
        }

        return {
            type: 'button',
            onAction: function onAction() {
                timesClicked++;
                pluginAPI.updateAttributes({
                    label: getLabel(),
                });
            },
            label: getLabel(),
        };
    }

    require(['@atlassian/clientside-extensions-registry'], function(registry) {
        registry.registerExtension('com.atlassian.plugins.atlassian-clientside-extensions-demo:button-plugin-js', buttonPluginFactory);
    });
})();
