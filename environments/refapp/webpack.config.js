const path = require('path');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const ClientsideExtensionsWebpackPlugin = require('@atlassian/clientside-extensions-webpack-plugin');
const webpack = require('webpack');
const merge = require('webpack-merge');

const providedDependencies = require('./provided-dependencies.json');

const FRONTEND_ROOT = path.join(__dirname, 'src/main/frontend');
const PROJECT_ROOT = path.resolve(__dirname, '../../');

const resolveRoot = relativePath => path.resolve(PROJECT_ROOT, relativePath);
const resolveFrontend = relativePath => path.resolve(FRONTEND_ROOT, relativePath);

// The build should go into the target directory, rather than the src
// __dirname returns directory that this file lives in and does not depend on location where node is called from.
const baseOutputDirectory = path.join(__dirname, 'target/classes');
const metadataOutputDirectory = path.resolve(baseOutputDirectory, 'META-INF/plugin-descriptors');

const getWrmPlugin = (watch = false, watchPrepare = false) =>
    /**
     * More info about WRM Webpack Plugin here:
     * https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin
     */
    new WrmPlugin({
        pluginKey: 'com.atlassian.plugins.atlassian-clientside-extensions-demo',

        // Path where WRM Plugin will store the generated XML file for us.
        xmlDescriptors: path.resolve(metadataOutputDirectory, 'wr-webpack-bundles.xml'),

        // Set the list of provided dependencies
        providedDependencies: watch ? {} : providedDependencies,
        addAsyncNameAsContext: true,
        contextMap: {
            testpage: 'client-plugins.demos.testpage',
            'watchmode-shim': 'client-plugins.demos.watchmode-shim',
        },
        watch,
        watchPrepare,
    });

const basicExtensions = new ClientsideExtensionsWebpackPlugin({
    cwd: resolveRoot('demos/demo-extensions-basic/src'),
    pattern: '**/*.{ts,tsx}',
    xmlDescriptors: path.resolve(metadataOutputDirectory, 'wr-extensions-basic.xml'),
});

const complexExtensions = new ClientsideExtensionsWebpackPlugin({
    cwd: resolveRoot('demos/demo-extensions-complex/src'),
    pattern: '**/*.{ts,tsx}',
    xmlDescriptors: path.resolve(metadataOutputDirectory, 'wr-extensions-complex.xml'),
});

const productExtensions = new ClientsideExtensionsWebpackPlugin({
    cwd: resolveRoot('demos/demo-product/src'),
    pattern: '**/*.{ts,tsx}',
    xmlDescriptors: path.resolve(metadataOutputDirectory, 'wr-extensions-product.xml'),
});

const inPluginExtensions = new ClientsideExtensionsWebpackPlugin({
    cwd: resolveFrontend('.'),
    pattern: '**/extensions/**/*.{ts,tsx}',
    xmlDescriptors: path.resolve(metadataOutputDirectory, 'wr-extensions-inplugin.xml'),
});

/**
 * Our very Basic Webpack configuration working with React
 * https://www.typescriptlang.org/docs/handbook/react-&-webpack.html
 */
const mode = process.env.NODE_ENV || 'development';
const baseConfig = {
    mode,
    node: false,
    context: resolveFrontend('.'),
    entry: {
        /** Provider of all plugin demos */
        testpage: resolveRoot('demos/demo-product/src/index.tsx'),

        /** Provide shim for provided "@atlassian/clientside-extensions-registry" in watchmode */
        'watchmode-shim': resolveFrontend('watchmode-shim/atlassian-runtime.js'),

        /** Consumers of plugin extension points */
        ...basicExtensions.generateEntrypoints(),
        ...complexExtensions.generateEntrypoints(),
        ...inPluginExtensions.generateEntrypoints(),
        ...productExtensions.generateEntrypoints(),
    },
    devtool: 'cheap-module-source-map',
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json'],
    },
    output: {
        // We build our code inside the target/classes folder.
        path: baseOutputDirectory,
        filename: '[name].bundle.js',
    },
    optimization: {
        minimize: false,
        runtimeChunk: 'single',
        namedModules: true,
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules|\/packages\//,
                    chunks: 'all',
                    name: 'vendor',
                    priority: 10,
                    enforce: true,
                },
            },
        },
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader',
                options: { errorsAsWarnings: true },
            },
        ],
    },
    plugins: [new webpack.NamedChunksPlugin()],
};

// bind on public interface
const bindHostname = '0.0.0.0';

const devServerPort = '3333';

const watchConfig = {
    devServer: {
        host: bindHostname,
        disableHostCheck: true,
        port: devServerPort,
        overlay: true,
        hot: true,
        headers: { 'Access-Control-Allow-Origin': '*' },
    },
    plugins: [new webpack.NamedModulesPlugin(), new webpack.HotModuleReplacementPlugin()],
};

const getGeneralWatchConfig = watchPrepare => ({
    output: {
        publicPath: `http://${bindHostname}:${devServerPort}/`,
        filename: '[name].js',
        chunkFilename: '[name].chunk.js',
    },
    plugins: [getWrmPlugin(true, watchPrepare), basicExtensions, complexExtensions, inPluginExtensions, productExtensions],
});

module.exports = env => {
    /**
     * Because of the way the watch-mode works we need to generate a plugin.xml file in "watch-mode".
     * However since the watch-mode also requires the `ajs-i18n-loader` which stalls due to open file-watchers,
     * we need to have a "watch-prepare" step, that does everything but running with the ajs-i18n-loader.
     *
     * That is ok, because we only need to run the watch-prepare when repackaging this plugin, in order to generate the
     * plugin.xml, the content of the build files is unimportant, as these will be surfed by the webpack-dev-server
     * which runs in "proper" watch-mode (including the afore mentioned loader).
     */
    if (env === 'watch_prepare') {
        return merge([baseConfig, getGeneralWatchConfig(true)]);
    }

    if (env === 'watch') {
        return merge([watchConfig, baseConfig, getGeneralWatchConfig(false)]);
    }

    return merge([
        baseConfig,
        {
            plugins: [getWrmPlugin(), basicExtensions, complexExtensions, inPluginExtensions, productExtensions],
            module: {
                rules: [{ enforce: 'pre', test: /\.js$/, loader: 'source-map-loader' }],
            },
        },
    ]);
};
