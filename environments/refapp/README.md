## Atlassian Client-side Extensions - Demo Refapp

Demonstrates how product developers and plugin developers can make use of the system's APIs.

### Running Demo

In monorepo root folder, run:

1. `mvn clean install` to build the whole project + demo.
2. `yarn demo/refapp` to start the demo.

### Consideration with this Demo

Since we're trying to create locations to render plugins and web-items using an Atlassian Plugin, and not directly modifying
the product (Reffapp in this case), we're using a regular WebPanel and a few JS resources to serve as the example of
how a product should implement the API.

`/resources/`

-   `atlassian-plugin.xml`: regular Altassian Plugin declaration. Here's the declaration of the WebPanel used for Demoing.
-   `web-panel.vm`: WebPanel's template, where the `#myPlugin` and `#myWebItems` divs are declared for demoing.

`/frontend``

-   `consumer`: demonstrate how Plugin Devs should define their plugin entrypoints and how to use the attributes API.
    This is used in combination with `atlassian-plugin.xml` where the web-items are defined.
-   `provider`: demonstrate how Products should provide plugin locations and how to use the default handlers
