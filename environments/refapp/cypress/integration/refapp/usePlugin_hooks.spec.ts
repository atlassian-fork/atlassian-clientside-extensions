/* eslint-disable-next-line spaced-comment */
/// <reference types="cypress" />

describe('Refapp usePlugin hooks', () => {
    beforeEach(() => {
        cy.visit('/');
    });

    it('successfully updates the "Current Value" counter', () => {
        cy.get('[data-testid=current-value]').should('contain.text', '0');
        cy.get('[data-testid=increment-current-value]').click();
        cy.get('[data-testid=current-value]').should('contain.text', '1');
        cy.get('[data-testid=decrement-current-value]').click();
        cy.get('[data-testid=current-value]').should('contain.text', '0');
    });

    it('successfully hides and shows locations', () => {
        cy.get('[data-testid=location-fragment]').should('exist');
        cy.get('[data-testid=show-hide-location]').click();
        cy.get('[data-testid=location-fragment]').should('not.exist');
        cy.get('[data-testid=show-hide-location]').click();
        cy.get('[data-testid=location-fragment]').should('exist');
    });

    it('successfully opens and closes Modal with action callback', () => {
        cy.contains('button', 'Open Modal: 0').click();
        cy.get('[data-testid=modal-with-action-callback]').should('exist');
        cy.contains('button', 'close').click();
        cy.get('[data-testid=modal-with-action-callback]').should('not.exist');
    });

    it('successfully opens Modal with action callback and requests update', () => {
        cy.contains('button', 'Open Modal: 0').click();
        cy.contains('button', 'request update').click();
        cy.get('[data-testid=modal-with-action-callback]').should('contain.text', 'requested update');
        cy.contains('button', 'close').click();
        cy.contains('button', 'Open Modal:').should('contain.text', '1');
    });

    it('successfully opens Modal with action callback and calls callback', () => {
        const stub = cy.stub();
        cy.on('window:confirm', stub);

        cy.contains('button', 'Open Modal: 0').click();
        // using the ESC keyboard shortcut to close the modal without clicking the close button to trigger
        // the onClose API.
        cy.get('[data-testid=client-plugins-modal]')
            .type('{esc}')
            .then(() => {
                expect(stub.getCall(0)).to.be.calledWith('Are you sure you want to close me? ☹️');
            });
    });

    it('successfully activates panel plugin with react with a "hi" and "bye"', () => {
        const stub = cy.stub();
        cy.on('window:alert', stub);

        cy.get('[data-testid=panel-plugin-with-react-hi-button]')
            .click()
            .then(() => {
                expect(stub.getCall(0)).to.be.calledWith('Hello from a React Panel! 😄');
            });

        cy.get('[data-testid=panel-plugin-with-react-bye-button]')
            .click()
            .then(() => {
                expect(stub.getCall(1)).to.be.calledWith('Bye bye from a React Panel! 👋');
            });
    });
});
