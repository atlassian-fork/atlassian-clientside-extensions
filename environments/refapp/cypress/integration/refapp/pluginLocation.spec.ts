/* eslint-disable-next-line spaced-comment */
/// <reference types="cypress" />

describe('Refapp pluginLocation component', () => {
    beforeEach(() => {
        cy.visit('/');
    });

    it('successfully loads link plugin and directs to an external website', () => {
        cy.get('[label="Go to Google"]')
            .should('have.attr', 'href')
            .and('include', 'http://www.google.com');
    });

    it('successfully increases counter when clicked in Button plugin', () => {
        cy.contains('button', 'Button clicked').should('contain.text', '0');
        cy.contains('button', 'Button clicked').click();
        cy.contains('button', 'Button clicked').should('contain.text', '1');
    });

    it('successfully open alert in Panel plugin', () => {
        const stub = cy.stub();
        cy.on('window:alert', stub);

        cy.get('[data-testid=panel-plugin-button]')
            .click()
            .then(() => {
                expect(stub.getCall(0)).to.be.calledWith('You clicked a button');
            });
    });

    it('successfully opens and closes modal in Modal plugin', () => {
        const stub = cy.stub();
        cy.on('window:confirm', stub);

        cy.contains('button', 'Modal with JS').click();
        cy.get('[data-testid=client-plugins-modal]').should('exist');
        cy.focused()
            .type('{esc}')
            .then(() => {
                expect(stub.getCall(0)).to.be.calledWith('Are you sure you want to close this modal?');
            });
        cy.get('[data-testid=client-plugins-modal]').should('not.exist');
    });

    it('successfully clicks Primary action in Modal plugin', () => {
        const stub = cy.stub();
        cy.on('window:confirm', stub);

        cy.contains('button', 'Modal with JS').click();
        cy.contains('button', 'Primary').click();
        cy.get('[data-testid=client-plugins-modal]').should('not.exist');
    });

    it('successfully clicks Secondary action in Modal plugin', () => {
        const stub = cy.stub();
        cy.on('window:confirm', stub);

        cy.contains('button', 'Modal with JS').click();
        cy.contains('button', 'Secondary').click();
        cy.get('[data-testid=client-plugins-modal]').should('contain.text', 'secondary clicked');
    });

    it('successfully opens link in Web item without entry', () => {
        cy.get('[label="Web item without entry"]').click();
        cy.url().should('include', '/my/fake/url/2');
    });
});
