/* eslint-disable no-underscore-dangle */

import { Observer } from '@atlassian/clientside-extensions-base';
import ReplaySubject from '@atlassian/clientside-extensions-base/dist/ReplaySubject';
import { observeDebugSubject, registerDebugSubject } from './debug-subjects';

enum ClientPluginDebugTypes {
    debug = 'debug',
    logging = 'logging',
    validation = 'validation',
    discovery = 'discovery',
}

export type ClientPluginDebug = {
    [key in ClientPluginDebugTypes]: boolean;
};

type ExposedClientPluginDebug = { [key in keyof ClientPluginDebug]: PropertyDescriptor } & {
    __initialized: PropertyDescriptor;
};

declare global {
    interface Window {
        ____c_p_d: ExposedClientPluginDebug;
    }
}

const defineDebugGlobal = (previousGlobal: unknown) => {
    // initialize as boolean
    const debugGlobal = Object.create(null);
    const debugStates: ClientPluginDebug = {
        debug: false,
        logging: false,
        validation: false,
        discovery: false,
    };

    const debugStateSubject = registerDebugSubject<ClientPluginDebug>('state', () => new ReplaySubject(1));

    const properties: ExposedClientPluginDebug = {
        debug: {
            get() {
                return debugStates.debug;
            },
            set(val: unknown) {
                debugStates.debug = Boolean(val);
                debugStateSubject.notify(debugStates);
            },
        },
        logging: {
            get() {
                return debugStates.logging;
            },
            set(val: unknown) {
                debugStates.logging = Boolean(val);
                debugStateSubject.notify(debugStates);
            },
        },
        validation: {
            get() {
                return debugStates.validation;
            },
            set(val: unknown) {
                debugStates.validation = Boolean(val);
                debugStateSubject.notify(debugStates);
            },
        },
        discovery: {
            get() {
                return debugStates.discovery;
            },
            set(val: unknown) {
                debugStates.discovery = Boolean(val);
                debugStateSubject.notify(debugStates);
            },
        },
        __initialized: { value: true, writable: false },
    };
    Object.defineProperties(debugGlobal, properties);

    Object.defineProperty(window, '____c_p_d', {
        configurable: true, // needed for testing :(
        get() {
            return debugGlobal;
        },
        set(val) {
            // its a boolean
            if (val === true || val === false) {
                debugGlobal.debug = val;
                debugGlobal.logging = val;
                debugGlobal.validation = val;
                debugGlobal.discovery = val;
                return;
            }

            // we can handle booleans - like above
            // or objects (except null) and nothing else.
            if (typeof val !== 'object' || val === null) {
                return;
            }

            // we now have an object
            type KeysOfClientPluginDebug = keyof ClientPluginDebug;
            (['debug', 'logging', 'validation', 'discovery'] as KeysOfClientPluginDebug[]).forEach(key => {
                if (key in val) {
                    debugGlobal[key] = val[key];
                }
            });
        },
    });
    window.____c_p_d = previousGlobal as ExposedClientPluginDebug;
};

if (!window.____c_p_d || !window.____c_p_d.__initialized) {
    defineDebugGlobal(window.____c_p_d || process.env.NODE_ENV !== 'production');
}

export const observeStateChange = (observer: Observer<ClientPluginDebug>) => observeDebugSubject<ClientPluginDebug>('state', observer);

export const isDebugEnabled = () => {
    return window.____c_p_d.debug as boolean;
};

export const isLoggingEnabled = () => {
    return window.____c_p_d.logging as boolean;
};

export const isValidationEnabled = () => {
    return window.____c_p_d.validation as boolean;
};

export const isDiscoveryEnabled = () => {
    return window.____c_p_d.discovery as boolean;
};

export const setDebugEnabled = (value: boolean) => {
    (window.____c_p_d.debug as boolean) = value;
};

export const setLoggingEnabled = (value: boolean) => {
    (window.____c_p_d.logging as boolean) = value;
};

export const setValidationEnabled = (value: boolean) => {
    (window.____c_p_d.validation as boolean) = value;
};

export const setDiscoveryEnabled = (value: boolean) => {
    (window.____c_p_d.discovery as boolean) = value;
};
