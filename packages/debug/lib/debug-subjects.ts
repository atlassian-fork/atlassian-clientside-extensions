/* eslint-disable no-underscore-dangle */
import { Observer, Subject } from '@atlassian/clientside-extensions-base';

type ExposedClientPluginSubjects = {
    __initialized: PropertyDescriptor;
    subjects: PropertyDescriptor;
};

type DebugSubjects = {
    [key: string]: Subject<unknown>;
};

declare global {
    interface Window {
        ____c_p_s: ExposedClientPluginSubjects;
    }
}

const defineSubjectGlobal = () => {
    // initialize as boolean
    const subjectGlobal = Object.create(null);

    const properties: ExposedClientPluginSubjects = {
        __initialized: { value: true, writable: false },
        subjects: {
            value: subjectGlobal,
            writable: false,
        },
    };
    Object.defineProperties(subjectGlobal, properties);

    window.____c_p_s = subjectGlobal;
};

if (!window.____c_p_s || !window.____c_p_s.__initialized) {
    defineSubjectGlobal();
}

export const registerDebugSubject = <T>(key: string, subjectFactory?: () => Subject<T>): Subject<T> => {
    // good old singletons - ensure we do not register the same subject twice.
    // we also can't fail as multiple instances might try to access the same, so instead we return the previously registered one.
    if (!(window.____c_p_s.subjects as DebugSubjects)[key]) {
        (window.____c_p_s.subjects as DebugSubjects)[key] = subjectFactory ? subjectFactory() : new Subject<T>();
    }
    return (window.____c_p_s.subjects as DebugSubjects)[key] as Subject<T>;
};

export const observeDebugSubject = <T>(key: string, observer: Observer<T>) => {
    if (!(window.____c_p_s.subjects as DebugSubjects)[key]) {
        throw new Error(`No subject registered for key: "${key}"`);
    }
    return (window.____c_p_s.subjects as DebugSubjects)[key].subscribe(observer);
};
