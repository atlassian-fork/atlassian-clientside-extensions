const observeDebugSubject = jest.fn();
const registerDebugSubject = jest.fn();

export { observeDebugSubject, registerDebugSubject };
