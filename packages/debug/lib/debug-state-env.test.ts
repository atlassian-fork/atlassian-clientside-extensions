/* eslint-disable  no-underscore-dangle */
/**
 * Using async imports in order to test process.env.NODE_ENV usage in module
 * https://stackoverflow.com/a/48042799
 */
describe('debug-state initialization', () => {
    const ORIGINAL_NODE_ENV = process.env.NODE_ENV;

    beforeEach(() => {
        // reset NODE_ENV to whatever it was.
        process.env.NODE_ENV = ORIGINAL_NODE_ENV;
        // reset cache of loaded modules. This will ensure that the module is re-initialized
        // with the new NODE_ENV value
        delete window.____c_p_d;
        jest.resetModules();
    });

    it('should be set to FALSE if process.env.NODE_ENV is "production"', async () => {
        process.env.NODE_ENV = 'production';

        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const debugStateModule = await import('./debug-state');

        expect(window.____c_p_d.__initialized).toBe(true);

        expect(window.____c_p_d.debug).toBe(false);
        expect(window.____c_p_d.logging).toBe(false);
        expect(window.____c_p_d.validation).toBe(false);
        expect(window.____c_p_d.discovery).toBe(false);
    });

    it('should be set to TRUE if process.env.NODE_ENV is different than "production"', async () => {
        process.env.NODE_ENV = 'test';
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const debugStateModule = await import('./debug-state');

        expect(window.____c_p_d.__initialized).toBe(true);

        expect(window.____c_p_d.debug).toBe(true);
        expect(window.____c_p_d.logging).toBe(true);
        expect(window.____c_p_d.validation).toBe(true);
        expect(window.____c_p_d.discovery).toBe(true);
    });
});
