import { Subject } from '@atlassian/clientside-extensions-base';
import { observeDebugSubject, registerDebugSubject } from './debug-subjects';

describe('debug-subject', () => {
    describe('registerDebugSubject', () => {
        it('should create an register a subject under a given namespace', () => {
            const subject = registerDebugSubject('some-key');
            expect(subject).toBeInstanceOf(Subject);
        });

        it('should not create a new instance for a previously registered key', () => {
            const subject = registerDebugSubject('some-new-key');
            const sameSubject = registerDebugSubject('some-new-key');
            expect(subject).toBe(sameSubject);
        });
    });

    describe('observeDebugSubject', () => {
        it('should throw if the subject does not exist yet', () => {
            const unregisteredSubject = () => observeDebugSubject('throw-subject', () => {});
            expect(unregisteredSubject).toThrow('No subject registered for key: "throw-subject"');
        });

        it('should subscribe to a previously registered subject', () => {
            const SUBJECT = 'working-example';
            const TEST_PAYLOAD = {
                some: 'payload',
            };
            const observer = jest.fn();
            const subject = registerDebugSubject(SUBJECT);
            observeDebugSubject(SUBJECT, observer);

            subject.notify(TEST_PAYLOAD);
            expect(observer).toBeCalledWith(TEST_PAYLOAD);
        });
    });
});
