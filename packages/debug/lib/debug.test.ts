import { mocked } from 'ts-jest/utils';
import { isDebugEnabled, isLoggingEnabled } from './debug-state';
import { LogLevel, observeLogger, onDebug } from './debug';

jest.mock('./debug-state');

describe('onDebug util', () => {
    beforeEach(() => {
        mocked(isDebugEnabled).mockClear();
        mocked(isLoggingEnabled).mockClear();
    });

    it('should only execute the given callback when debug is enabled', () => {
        const debugCallbackSpy = jest.fn();

        // disable
        mocked(isDebugEnabled).mockReturnValueOnce(false);

        onDebug(debugCallbackSpy);

        expect(debugCallbackSpy).not.toHaveBeenCalled();

        // enable
        mocked(isDebugEnabled).mockReturnValueOnce(true);

        onDebug(debugCallbackSpy);

        expect(debugCallbackSpy).toHaveBeenCalled();
    });

    it('should not notify the observer when logging is disabled', () => {
        const payload = {
            message: 'foo',
            level: LogLevel.error,
        };
        const loggerCallback = jest.fn(() => payload);
        const loggerObserver = jest.fn();

        // need to enable debug in order to run the debugCallback
        mocked(isDebugEnabled).mockReturnValue(true);
        // disable
        mocked(isLoggingEnabled).mockReturnValueOnce(false);

        observeLogger(loggerObserver);
        onDebug(loggerCallback);

        expect(loggerCallback).toBeCalledTimes(1);
        expect(loggerObserver).not.toBeCalled();

        // enable
        mocked(isLoggingEnabled).mockReturnValueOnce(true);
        onDebug(loggerCallback);

        expect(loggerCallback).toBeCalledTimes(2);
        expect(loggerObserver).toBeCalledWith(payload);
    });
});
