/**
 * We share a logger with the callback that the consumer can use.
 * For now, we are using `console` directly, but we might proxy it latter to add more info.
 */
import { Observer, ReplaySubject } from '@atlassian/clientside-extensions-base';
import { isDebugEnabled, isLoggingEnabled } from './debug-state';
import { observeDebugSubject, registerDebugSubject } from './debug-subjects';

export enum LogLevel {
    debug = 'DEBUG',
    info = 'INFO',
    warn = 'WARN',
    error = 'ERROR',
    trace = 'TRACE',
}

export type LoggerPayload = {
    level: LogLevel;
    message: string;
    components?: string | string[];
    meta?: {
        [key: string]: unknown;
    };
};

type LoggerCallback = (levels: typeof LogLevel) => LoggerPayload;

const debuggerSubject = registerDebugSubject<LoggerPayload>('logger', () => new ReplaySubject(20));

export const observeLogger = (observer: Observer<LoggerPayload>) => observeDebugSubject<LoggerPayload>('logger', observer);

const onDebug = (callback: LoggerCallback) => {
    if (isDebugEnabled()) {
        const payload = callback(LogLevel);
        if (payload && isLoggingEnabled()) {
            debuggerSubject.notify(payload);
        }
    }
};

// eslint-disable-next-line import/prefer-default-export
export { onDebug };
