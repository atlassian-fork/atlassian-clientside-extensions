/* eslint-disable no-underscore-dangle, jest/no-standalone-expect */
import cases from 'jest-in-case';
import { isDebugEnabled, isDiscoveryEnabled, isLoggingEnabled, isValidationEnabled } from './debug-state';

describe('is* utils', () => {
    beforeEach(() => {
        (window.____c_p_d as unknown) = false;
    });

    cases(
        'should be active if respective flag is set to TRUE',
        opts => {
            expect(opts.isUtil()).toBe(false);

            // switch flag to TRUE
            (window.____c_p_d as unknown) = { [opts.key]: true };

            expect(opts.isUtil()).toBe(true);

            // switch ALL false
            (window.____c_p_d as unknown) = false;

            expect(opts.isUtil()).toBe(false);

            // switch ALL true
            (window.____c_p_d as unknown) = true;

            expect(opts.isUtil()).toBe(true);
        },
        {
            isDebugEnabled: { isUtil: isDebugEnabled, key: 'debug' },
            isLoggingEnabled: { isUtil: isLoggingEnabled, key: 'logging' },
            isValidationEnabled: { isUtil: isValidationEnabled, key: 'validation' },
            isDiscoveryEnabled: { isUtil: isDiscoveryEnabled, key: 'discovery' },
        },
    );
});
