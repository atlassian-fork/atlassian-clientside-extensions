# Client-side Extensions

## Webpack plugin

A webpack plugin to faciliate the consumption of client-side extensions using webpack. It will read comment annotations to create the
necesary web-fragments XML declarations for plugin devs.

Refer to the [official documentation](https://developer.atlassian.com/server/framework/clientside-extensions) for more information.
