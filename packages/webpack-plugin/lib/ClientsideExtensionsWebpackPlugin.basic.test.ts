import fs = require('fs');
import path = require('path');
import webpack = require('webpack');

import WrmPlugin = require('atlassian-webresource-webpack-plugin');
import Plugin = require('./ClientsideExtensionsWebpackPlugin');

const inputDir = path.resolve(__dirname, '__fixtures__/basic/');
const outputDir = path.resolve(__dirname, '__fixtures__/target/');

describe('basic compilation', () => {
    // @ts-ignore
    const basicWrmPlugin = new WrmPlugin({
        pluginKey: 'a.fake.plugin.key',
        xmlDescriptors: path.resolve(outputDir, 'wrm.xml'),
    });
    // @ts-ignore
    const basicPlugin = new Plugin({
        cwd: inputDir,
        pattern: '**/extension*.js',
    });
    // base config for all tests
    const baseWebpackConfig = {
        entry: {
            first: path.resolve(inputDir, 'first.js'),
            second: path.resolve(inputDir, 'second.js'),
            ...basicPlugin.generateEntrypoints(),
        },
        plugins: [basicWrmPlugin, basicPlugin],
        output: {
            path: outputDir,
        },
    };

    beforeAll(done => {
        webpack(baseWebpackConfig, () => done());
    });

    it("generates an xml file next to the wrm plugin's output", () => {
        const wrmXmlFile = path.join(outputDir, 'wrm.xml');
        expect(fs.existsSync(wrmXmlFile)).toBe(true);

        const ourXmlFile = path.join(outputDir, 'wr-generated-clientside-extensions.xml');
        expect(fs.existsSync(ourXmlFile)).toBe(true);
    });

    it('discovers clientside extensions', () => {
        const xmlFile = path.join(outputDir, 'wr-generated-clientside-extensions.xml');
        const xmlFileContent = fs.readFileSync(xmlFile);
        const foundExtensions = String(xmlFileContent).match(/<web-item/g);
        expect(foundExtensions).not.toBe(null);
        expect(foundExtensions.length).toBe(3);
    });

    it('outputs compiled extension code to an appropriate location', () => {
        const compiledPlugin1 = path.join(outputDir, 'extension-1-js.js');
        const compiledPlugin2 = path.join(outputDir, 'extension-2-js.js');
        const nestedCompiledPlugin = path.join(outputDir, 'nested-extension-42-js.js');

        expect(fs.existsSync(compiledPlugin1)).toBe(true);
        expect(fs.existsSync(compiledPlugin2)).toBe(true);
        expect(fs.existsSync(nestedCompiledPlugin)).toBe(true);
    });
});
