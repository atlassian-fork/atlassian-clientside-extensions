declare module 'pretty-data' {
    interface IPrettyData {
        xml(rawXmlString: string): string;
    }

    // eslint-disable-next-line import/prefer-default-export
    export const pd: IPrettyData;
}
