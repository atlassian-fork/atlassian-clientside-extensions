import { Comment, Tag } from 'comment-parser';

const isNumber = (maybeNumber: unknown) => !Number.isNaN(maybeNumber as number);

type ConditionReducerFn = (aggregator: { [key: string]: object }, tag: Tag) => void;
type ConditionReduction = { [key: string]: ConditionReduction | string };

const reduceToConditionsPayload = (tag: Tag, reducerFn: ConditionReducerFn): ConditionReduction => {
    return tag.description
        ? tag.description
        : tag.tags.reduce((agg, tag) => {
              reducerFn(agg, tag);
              return agg;
          }, {});
};

const resultToArrayOrObject = (result: ConditionReduction): ConditionReduction | (ConditionReduction | string)[] => {
    const isNumberIndexed = typeof result === 'object' && Object.keys(result).every(key => isNumber(key));
    return isNumberIndexed ? Object.values(result) : result;
};

const toConditionObject: ConditionReducerFn = (obj, tag) => {
    const result = reduceToConditionsPayload(tag, toConditionObject);
    obj[tag.name] = resultToArrayOrObject(result);
};

type ConditionComment = { tags: Tag[] };
const extractConditionFromComments = (comment: Comment) => {
    return comment.tags
        .filter(tag => tag.tag === 'condition')
        .reduce(
            (agg, condition) => {
                agg.tags.push(condition);
                return agg;
            },
            { tags: [] } as ConditionComment,
        );
};

const removeConditionFromComment = (comment: Comment) => {
    return {
        ...comment,
        tags: comment.tags.filter(tag => tag.tag !== 'condition'),
    } as Comment;
};

const getCondition = (comment: ConditionComment): ConditionReduction | null => {
    if (comment.tags.length === 0) {
        return null;
    }
    const rootObject = {};
    comment.tags.map(tag => toConditionObject(rootObject, tag));

    return rootObject as ConditionReduction;
};

export type TransformedComment = { [key: string]: string } & { condition?: ConditionReduction | string };
export default (comment: Comment) => {
    const conditionComments = extractConditionFromComments(comment);
    const condition = getCondition(conditionComments);
    const restComment = removeConditionFromComment(comment);
    const payload = restComment.tags.reduce((agg, { tag, name }) => {
        agg[tag] = name;
        return agg;
    }, {} as TransformedComment);

    if (condition !== null) {
        payload.condition = condition;
    }

    return payload;
};
