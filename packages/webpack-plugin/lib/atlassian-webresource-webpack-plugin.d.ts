declare module 'atlassian-webresource-webpack-plugin/src/helpers/xml' {
    interface IRenderElement {
        (elementName: string, attributes: object, children?: string | Array<string>): string;
    }
    export const renderElement: IRenderElement;
}

declare module 'atlassian-webresource-webpack-plugin/src/renderCondition' {
    export default function(conditions: object | undefined): string;
}

class IWrmPlugin {
    public constructor(options: object);
}

declare module 'atlassian-webresource-webpack-plugin' {
    export default IWrmPlugin;
}
