import { ButtonExtension } from '../../../../clientside-extensions';

/**
 * @clientside-extension
 * @extension-point bar.baz
 */
ButtonExtension.factory(() => ({ label: 'My second extension' }));
