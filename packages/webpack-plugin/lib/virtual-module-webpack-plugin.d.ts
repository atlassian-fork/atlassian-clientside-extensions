declare module 'virtual-module-webpack-plugin' {
    import { Compiler } from 'webpack';

    interface IVirtualModuleParams {
        moduleName: string;
        contents: string;
    }

    export default class VirtualModuleWebpackPlugin {
        constructor(params: IVirtualModuleParams);

        apply(compiler: Compiler): void;
    }
}
