import fs from 'fs';
import path from 'path';
import { ClientsideExtensionsOptions } from '../types';

const rawEntrypointCode = fs.readFileSync(path.join(__dirname, '..', '..', 'runtime', 'entrypoint.js'), 'utf8');

export default (options: ClientsideExtensionsOptions): string => {
    return rawEntrypointCode
        .replace(/REPLACE_WITH_PATHNAME/g, options.filePath)
        .replace(/REPLACE_WITH_PLUGIN_ID/g, options.fullyQualifiedNamespace);
};
