import fs from 'fs';
import path from 'path';

import { Compiler, Output, Plugin } from 'webpack';
import parseComment from 'comment-parser';
import glob from 'glob';
import kebabCase from 'lodash.kebabcase';
import { pd as PrettyData } from 'pretty-data';
import VirtualModulePlugin from 'virtual-module-webpack-plugin';
import commentTransformer, { TransformedComment } from './comment-transformer';

import generateWebpanelDescriptor from './generator/webpanel-descriptor';
import generateEntrypointCode from './generator/webpanel-entrypoint';
import { ClientsideExtensionsManifest, ClientsideExtensionsOptions, IWrmPluginOptions } from './types';

interface IClientsideExtensionsPluginOptions {
    pattern?: string;
    cwd?: string;
    xmlDescriptors?: string;
}

export = class ClientsideExtensionsWebpackPlugin {
    private readonly pattern: string;

    private readonly cwd: string;

    private readonly outputFilename: string;

    private manifests: ClientsideExtensionsManifest[];

    constructor(options: IClientsideExtensionsPluginOptions = {}) {
        if (!options.pattern) {
            throw new Error('No pattern was specified to match clientside extensions');
        }

        this.outputFilename = options.xmlDescriptors;
        this.pattern = options.pattern;
        this.cwd = options.cwd || process.cwd();

        this.manifests = ClientsideExtensionsWebpackPlugin.findFilesByPattern(this.pattern, this.cwd)
            .map(file => ClientsideExtensionsWebpackPlugin.parseManifestFromComment(file, this.cwd))
            .filter(Boolean);
    }

    public generateEntrypoints() {
        return this.manifests.reduce((agg, manifest) => {
            const id = ClientsideExtensionsWebpackPlugin.getIdFromManifestKey(manifest.key);
            agg[id] = ClientsideExtensionsWebpackPlugin.getFilenameForEntry(id);

            return agg;
        }, {} as { [key: string]: string });
    }

    apply(compiler: Compiler) {
        const wrmOptions = ClientsideExtensionsWebpackPlugin.getWrmWebpackPluginOptions(compiler);

        const extensionsOptions = this.manifests.map(manifest => {
            return ClientsideExtensionsWebpackPlugin.manifestToPluginOptions(wrmOptions.pluginKey, manifest);
        });

        // side effect
        ClientsideExtensionsWebpackPlugin.addContextToEntrypoints(extensionsOptions, wrmOptions);

        // side effect
        ClientsideExtensionsWebpackPlugin.addConditionToEntrypoint(extensionsOptions, wrmOptions);

        extensionsOptions.forEach(extensionOption => {
            new VirtualModulePlugin({
                moduleName: ClientsideExtensionsWebpackPlugin.getFilenameForEntry(extensionOption.id),
                contents: generateEntrypointCode(extensionOption),
            }).apply(compiler);
        });

        compiler.hooks.emit.tapAsync('Generate clientside-extension XML', (compilation, callback) => {
            const webpanelDescriptors = extensionsOptions.map(extensionOption => generateWebpanelDescriptor(extensionOption));
            const xmlDescriptors = PrettyData.xml(`<clientside-extension>${webpanelDescriptors.join('')}</clientside-extension>`);

            const xmlDescriptorDirname = path.dirname(
                path.relative((compiler.options.output as Output).path || '', this.outputFilename || wrmOptions.xmlDescriptors),
            );
            const xmlDescriptorFilename = path.basename(this.outputFilename || 'wr-generated-clientside-extensions.xml');
            const webpanelXmlDescriptorFilename = path.join(xmlDescriptorDirname, xmlDescriptorFilename);

            compilation.assets[webpanelXmlDescriptorFilename] = {
                source: () => Buffer.from(xmlDescriptors),
                size: () => Buffer.byteLength(xmlDescriptors),
            };

            callback();
        });
    }

    private static addContextToEntrypoints(extensionOptions: ClientsideExtensionsOptions[], wrmOptions: IWrmPluginOptions) {
        extensionOptions.forEach(extensionOption => {
            const contextMap = wrmOptions.contextMap as Map<string, string[]>;
            contextMap.set(ClientsideExtensionsWebpackPlugin.getIdFromManifestKey(extensionOption.key), [extensionOption.section]);
        });
    }

    private static addConditionToEntrypoint(extensionOptions: ClientsideExtensionsOptions[], wrmOptions: IWrmPluginOptions) {
        extensionOptions
            .filter(extensionOption => Boolean(extensionOption.condition))
            .forEach(extensionOption => {
                const conditionMap = wrmOptions.conditionMap as Map<string, string[]>;
                conditionMap.set(
                    ClientsideExtensionsWebpackPlugin.getIdFromManifestKey(extensionOption.key),
                    (extensionOption.condition as unknown) as string[],
                );
            });
    }

    private static translateAttributes(rawManifest: { [key: string]: string }) {
        const { 'extension-point': extensionPoint, ...rest } = rawManifest;
        return {
            section: extensionPoint,
            ...rest,
        };
    }

    private static getMissingRequiredManifestAttributes(manifestObject: TransformedComment) {
        return ['extension-point'].filter(attrib => !manifestObject[attrib]);
    }

    private static manifestObjectToClientsideExtensionManifest(
        filePath: string,
        cwd: string,
        manifestObject: TransformedComment,
    ): ClientsideExtensionsManifest {
        const { key: maybeKey, section, label, link, weight, condition } = manifestObject;
        const key = ClientsideExtensionsWebpackPlugin.ensureKey(maybeKey, filePath, cwd);
        const id = ClientsideExtensionsWebpackPlugin.getIdFromManifestKey(key);
        const clientsideExtensionManifest: ClientsideExtensionsManifest = {
            id,
            filePath,
            key,
            section,
        };

        if (label) {
            clientsideExtensionManifest.label = label;
        }
        if (link) {
            clientsideExtensionManifest.link = link;
        }

        const parsedWeight = parseInt(weight, 10);
        if (weight && !Number.isNaN(parsedWeight)) {
            clientsideExtensionManifest.weight = parsedWeight;
        }

        if (condition) {
            clientsideExtensionManifest.condition = condition;
        }

        return clientsideExtensionManifest;
    }

    private static parseManifestFromComment(file: string, cwd: string): ClientsideExtensionsManifest | null {
        const comments = parseComment(fs.readFileSync(file, 'utf8'));

        // get first the clientside-extension comment
        const clientsiteExtensionComment = comments.find(comment => comment.tags.some(({ tag }) => tag === 'clientside-extension'));
        if (!clientsiteExtensionComment) {
            console.warn(`Client-side Extension: The file: "${file}" does not contain meta comments. Skipping XML-descriptor generation.`);
            return null;
        }

        const rawManifest = commentTransformer(clientsiteExtensionComment);
        const missingAttributes = ClientsideExtensionsWebpackPlugin.getMissingRequiredManifestAttributes(rawManifest);
        if (missingAttributes.length) {
            console.warn(
                `Client-side Extension: The manifest in file: "${file}" misses the following attributes: "${missingAttributes.join(
                    ', ',
                )}". Skipping XML-descriptor generation.`,
            );
            return null;
        }
        const transformedRawManifest = ClientsideExtensionsWebpackPlugin.translateAttributes(rawManifest);

        return ClientsideExtensionsWebpackPlugin.manifestObjectToClientsideExtensionManifest(file, cwd, transformedRawManifest);
    }

    private static findFilesByPattern(pattern: string, cwd: string) {
        return glob.sync(pattern, { absolute: true, cwd });
    }

    private static getWrmWebpackPluginOptions(compiler: Compiler): IWrmPluginOptions {
        const { plugins } = compiler.options;
        if (!plugins) {
            throw new Error(`Something went mighty wrong. Can't find any plugins in webpack`);
        }
        const wrmPlugin = plugins.find(p => p.constructor.name === 'WrmPlugin');

        if (!wrmPlugin) {
            throw new Error(
                // eslint-disable-next-line max-len
                `Can't find the WrmPlugin. Make sure you are using this plugin only in conjunction with the atlassian-webresource-webpack-plugin`,
            );
        }

        return (wrmPlugin as Plugin & { options: IWrmPluginOptions }).options;
    }

    private static getFilenameForEntry(manifestId: string) {
        return `./generated-clientside-extension/${manifestId}.js`;
    }

    private static ensureKey(key: string | undefined, fileName: string, cwd: string) {
        if (key) {
            return key;
        }

        return path.relative(cwd, fileName);
    }

    private static manifestToPluginOptions(pluginKey: string, manifest: ClientsideExtensionsManifest): ClientsideExtensionsOptions {
        const { condition, filePath, id, key, label, section, weight } = manifest;
        const resourceKey = `entrypoint-${id}`;

        return {
            condition,
            filePath,
            id,
            key,
            label,
            pluginKey,
            section,
            weight,
            entryPoint: resourceKey,
            fullyQualifiedNamespace: `${pluginKey}:${key}`,
        };
    }

    private static getIdFromManifestKey(key: string) {
        return kebabCase(key);
    }
};
