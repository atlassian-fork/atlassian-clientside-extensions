import registry from '@atlassian/clientside-extensions-registry';
// eslint-disable-next-line import/no-unresolved
import plugin from 'REPLACE_WITH_PATHNAME';

// we are not in watch mode
registry.registerExtension('REPLACE_WITH_PLUGIN_ID', plugin);

// we are in watch mode
if (module.hot) {
    module.hot.accept('REPLACE_WITH_PATHNAME', () => {
        // eslint-disable-next-line global-require,import/no-unresolved
        registry.registerExtension('REPLACE_WITH_PLUGIN_ID', require('REPLACE_WITH_PATHNAME').default);
    });
}
