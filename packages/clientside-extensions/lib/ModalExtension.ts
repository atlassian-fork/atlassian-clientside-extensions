import { ModalHandler } from '@atlassian/clientside-extensions-components';
import { PluginAttributesProvider } from '@atlassian/clientside-extensions-registry';

export const factory = (provider: PluginAttributesProvider): PluginAttributesProvider => {
    return (...args) => {
        const attributes = provider(...args);
        const onAction = attributes.onAction as ModalHandler.ModalRenderExtension;
        return {
            ...attributes,
            type: 'modal',
            onAction,
        };
    };
};

export class Api extends ModalHandler.Api {}
export type MountProps = ModalHandler.MountProps;
// Can't re-export Enums, or extend them. Need to implement this weird hack:
// https://github.com/microsoft/TypeScript/issues/17592#issuecomment-561065132
type Width = ModalHandler.ModalWidth;
export const Width = { ...ModalHandler.ModalWidth };
type Appearance = ModalHandler.ModalAppearance;
export const Appearance = { ...ModalHandler.ModalAppearance };
