import ReactDOM from 'react-dom';
import React, { ComponentType } from 'react';

export interface ElementRenderer {
    onMount(mountCallback: (element: HTMLElement, options: unknown) => void): ElementRenderer;
    onUnmount(unmountCallback: (element: HTMLElement) => void): ElementRenderer;
}

const renderElementAsReact = <T extends {}>(
    renderApi: ElementRenderer,
    RenderElement: ComponentType<{ options: unknown } & Omit<T, 'options'>>,
    additionalProps?: Omit<T, 'options'>,
) => {
    renderApi
        .onMount((element, options: unknown) => {
            // eslint-disable-next-line react/jsx-props-no-spreading
            ReactDOM.render(<RenderElement options={options} {...additionalProps} />, element);
        })
        .onUnmount(element => {
            ReactDOM.unmountComponentAtNode(element);
        });

    return this;
};

export default renderElementAsReact;
