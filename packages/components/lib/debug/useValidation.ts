import { useEffect, useState } from 'react';
import { isValidationEnabled, setValidationEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';

export default () => {
    const [validationState, setValidationState] = useState(isValidationEnabled());

    useEffect(() => {
        const subscription = observeStateChange(newState => {
            setValidationState(newState.validation);
        });
        return () => subscription.unsubscribe();
    }, []);

    return [validationState, setValidationEnabled] as [boolean, (value: boolean) => void];
};
