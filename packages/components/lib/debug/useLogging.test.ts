import { mocked } from 'ts-jest/utils';
import { act, renderHook } from '@testing-library/react-hooks';
import { isLoggingEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';
import useLogging from './useLogging';

jest.mock('@atlassian/clientside-extensions-debug');

describe('useLogging hook', () => {
    beforeEach(() => {
        mocked(isLoggingEnabled).mockClear();
        mocked(observeStateChange).mockClear();
    });

    it('should return the current state of the logging debug setting', () => {
        const INITIAL_VALUE = false;
        mocked(isLoggingEnabled).mockReturnValueOnce(INITIAL_VALUE);
        mocked(observeStateChange).mockReturnValueOnce({ unsubscribe() {} });

        const { result } = renderHook(() => useLogging());
        const [loggingValue] = result.current;

        expect(loggingValue).toBe(INITIAL_VALUE);
    });

    it('should react to changes to the logging debug setting', () => {
        const INITIAL_VALUE = true;
        const CHANGE_VALUE = false;
        let updateCallback: Parameters<typeof observeStateChange>[0];
        mocked(isLoggingEnabled).mockReturnValueOnce(INITIAL_VALUE);
        mocked(observeStateChange).mockImplementation(expectedCallback => {
            updateCallback = expectedCallback;
            return {
                unsubscribe() {},
            };
        });

        const { result } = renderHook(() => useLogging());
        let [loggingValue] = result.current;

        expect(loggingValue).toBe(INITIAL_VALUE);

        act(() => {
            updateCallback({ discovery: CHANGE_VALUE, debug: CHANGE_VALUE, logging: CHANGE_VALUE, validation: CHANGE_VALUE });
        });

        [loggingValue] = result.current;

        expect(loggingValue).toBe(CHANGE_VALUE);
    });
});
