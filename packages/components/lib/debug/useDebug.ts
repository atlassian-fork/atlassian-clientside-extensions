import { useEffect, useState } from 'react';
import { isDebugEnabled, setDebugEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';

export default () => {
    const [debugState, setDebugState] = useState(isDebugEnabled());

    useEffect(() => {
        const subscription = observeStateChange(newState => {
            setDebugState(newState.debug);
        });
        return () => subscription.unsubscribe();
    }, []);

    return [debugState, setDebugEnabled] as [boolean, (value: boolean) => void];
};
