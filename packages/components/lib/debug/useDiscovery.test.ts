import { mocked } from 'ts-jest/utils';
import { act, renderHook } from '@testing-library/react-hooks';
import { isDiscoveryEnabled, observeStateChange } from '@atlassian/clientside-extensions-debug';
import useDiscovery from './useDiscovery';

jest.mock('@atlassian/clientside-extensions-debug');

describe('useDiscovery hook', () => {
    beforeEach(() => {
        mocked(isDiscoveryEnabled).mockClear();
        mocked(observeStateChange).mockClear();
    });

    it('should return the current state of the discover debug setting', () => {
        const INITIAL_VALUE = false;
        mocked(isDiscoveryEnabled).mockReturnValueOnce(INITIAL_VALUE);
        mocked(observeStateChange).mockReturnValueOnce({ unsubscribe() {} });

        const { result } = renderHook(() => useDiscovery());
        const [discoveryValue] = result.current;

        expect(discoveryValue).toBe(INITIAL_VALUE);
    });

    it('should react to changes to the discover debug setting', () => {
        const INITIAL_VALUE = true;
        const CHANGE_VALUE = false;
        let updateCallback: Parameters<typeof observeStateChange>[0];
        mocked(isDiscoveryEnabled).mockReturnValueOnce(INITIAL_VALUE);
        mocked(observeStateChange).mockImplementation(expectedCallback => {
            updateCallback = expectedCallback;
            return {
                unsubscribe() {},
            };
        });

        const { result } = renderHook(() => useDiscovery());
        let [discoveryValue] = result.current;

        expect(discoveryValue).toBe(INITIAL_VALUE);

        act(() => {
            updateCallback({ discovery: CHANGE_VALUE, debug: CHANGE_VALUE, logging: CHANGE_VALUE, validation: CHANGE_VALUE });
        });

        [discoveryValue] = result.current;

        expect(discoveryValue).toBe(CHANGE_VALUE);
    });
});
