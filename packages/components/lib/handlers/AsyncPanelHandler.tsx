import React, { FunctionComponent, lazy, Suspense } from 'react';
import { onDebug } from '@atlassian/clientside-extensions-debug';
import { PanelHandlerProps, PanelRenderer, PanelRenderExtension } from './PanelHandler';

export const type = 'async-panel';

export type AsyncPanelRenderExtension = () => Promise<{ default: PanelRenderExtension; __esModule: boolean }>;

interface AsyncPanelHandlerProps {
    fallback: JSX.Element;
    location: string;
    pluginKey: string;
    renderProvider: AsyncPanelRenderExtension;
    data?: object;
    RootType?: PanelHandlerProps['RootType'];
}

export const AsyncPanelRenderer: FunctionComponent<AsyncPanelHandlerProps> = ({
    data = {},
    fallback,
    location,
    pluginKey,
    renderProvider,
    RootType,
}) => {
    const AsyncComponent = lazy(() => {
        try {
            return renderProvider().then(resolvedModule => {
                // eslint-disable-next-line no-underscore-dangle
                if (!resolvedModule || !resolvedModule.__esModule || typeof resolvedModule.default !== 'function') {
                    onDebug(({ error }) => ({
                        level: error,
                        // eslint-disable-next-line max-len
                        message: `The provided dynamic import of plugin "${pluginKey}" as location "${location}" does not look like a module that can be asynchronously loaded.`,
                        components: ['AsyncPanelRenderer'],
                        meta: {
                            location,
                            pluginKey,
                        },
                    }));
                }

                return {
                    default: () => {
                        return <PanelRenderer render={resolvedModule.default} RootType={RootType} data={data} />;
                    },
                };
            });
        } catch (e) {
            onDebug(({ error }) => ({
                level: error,
                // eslint-disable-next-line max-len
                message: `Failed trying to execute the dynamic import from plugin "${pluginKey}" for the async panel at location "${location}"`,
                components: ['AsyncPanelRenderer'],
                meta: {
                    location,
                    pluginKey,
                    error: e,
                },
            }));
            return null;
        }
    });

    return (
        <Suspense fallback={fallback}>
            {/* eslint-disable-next-line react/jsx-props-no-spreading */}
            <AsyncComponent />
        </Suspense>
    );
};
