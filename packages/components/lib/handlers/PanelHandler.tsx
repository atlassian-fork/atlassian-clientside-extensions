import React, { FunctionComponent, useEffect, useMemo, useRef } from 'react';

type PanelRenderCallback = (element: HTMLElement, options: {}) => void;

type PanelCleanupCallback = (element: HTMLElement) => void;

const noop = () => {};

export class Api {
    private renderCallback: PanelRenderCallback = noop;

    private cleanupCallback: PanelCleanupCallback = noop;

    public onMount(callback: PanelRenderCallback) {
        this.renderCallback = callback;

        return this;
    }

    public onUnmount(callback: PanelCleanupCallback) {
        this.cleanupCallback = callback;

        return this;
    }

    public getRenderCallback() {
        return this.renderCallback;
    }

    public getCleanupCallback() {
        return this.cleanupCallback;
    }
}

export type PanelRenderExtension = (api: Api, data?: object) => {};

export interface PanelHandlerProps {
    render: PanelRenderExtension;
    RootType?: 'div' | 'span';
    data?: object;
}

export type MountProps = {
    options: {};
};

export const type = 'panel';

export const PanelRenderer: FunctionComponent<PanelHandlerProps> = ({ render, RootType = 'div', data = {} }) => {
    const panelApi = useMemo(() => {
        const staticModalApi = new Api();
        render(staticModalApi, data);
        return staticModalApi;
    }, [render]);

    const ref = useRef(null);

    useEffect(() => {
        if (ref.current) {
            panelApi.getRenderCallback()(ref.current, {});
        }

        return () => {
            panelApi.getCleanupCallback()(ref.current);
        };
    }, [panelApi]);

    return <RootType ref={ref} />;
};
