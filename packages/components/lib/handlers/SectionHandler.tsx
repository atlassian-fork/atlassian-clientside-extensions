import React, { FC } from 'react';
import { PluginDescriptor } from '@atlassian/clientside-extensions-registry';
import useDebug from '../debug/useDebug';

export const type = 'section';

export const SectionRenderer: FC<{ extension: PluginDescriptor }> = ({ extension, children }) => {
    const [debug] = useDebug();
    if (debug) {
        return <div>{JSON.stringify(extension)}</div>;
    }
    return <>{children}</>;
};
