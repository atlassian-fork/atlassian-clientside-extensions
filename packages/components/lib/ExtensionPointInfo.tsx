import React, { useState } from 'react';
import Modal, { ModalTransition } from '@atlaskit/modal-dialog';

import { keyframes } from '@emotion/core';
import styled from '@emotion/styled';
// eslint-disable-next-line import/no-extraneous-dependencies
import { JSONSchema7 } from 'json-schema';
import useDiscovery from './debug/useDiscovery';
import { Options } from './useExtensions';

const InfoContainer = styled.div`
    max-height: 448px;
    overflow: auto;
`;

const ExtensionPointName = styled.div`
    font-size: 18px;
    font-style: italic;
    margin: 0 0 12px;
`;

const BlinkerContainer = styled.div`
    display: inline-block;
    position: relative;
`;

const blinkAnimation = keyframes`
    from {
        transform: scale(1);
    }

    to {
        transform: scale(1.3);
    }
`;

const Blinker = styled.div`
    height: 22px;
    width: 22px;
    background: rgba(7, 71, 166, 0.5);
    border-radius: 50%;
    position: absolute;
    left: 10px;
    top: -20px;
    animation: ${blinkAnimation} 1s ease-in-out infinite;
    animation-direction: alternate;

    &:after {
        display: block;
        background: rgb(7, 71, 166, 0.8);
        position: absolute;
        height: 12px;
        width: 12px;
        content: '';
        left: 50%;
        top: 50%;
        margin: -6px 0 0 -6px;
        border-radius: 50%;
    }
`;

const Property = styled.div`
    margin: 0 0 10px;
    border-bottom: 1px solid #ccc;
`;

const PropertyName = styled.div`
    font-size: 16px;
    margin: 0 0 10px;
`;

const PropertyDescriptorContainer = styled.div`
    margin: 6px 0;
    font-size: 12px;
`;

const PropertyValue = styled.span`
    color: #828282;
`;

const NestedProperty = styled.div`
    margin: 0 0 0 8px;
`;

interface Props {
    name: string;
    schemas: Options;
}

const PropertyDescriptor = ({ label, value }: { label: string; value?: string }) => {
    if (!value) {
        return null;
    }

    return (
        <PropertyDescriptorContainer>
            {label}: <PropertyValue>{value}</PropertyValue>
        </PropertyDescriptorContainer>
    );
};

function renderSchemaNode(node: JSONSchema7) {
    const { properties } = node;
    const required = node.required || [];

    return Object.keys(properties).map(propertyName => {
        const property = properties[propertyName] as JSONSchema7;

        return (
            <Property key={propertyName}>
                <PropertyName>{propertyName}</PropertyName>
                <PropertyDescriptor label="Type" value={property.type as string} />
                <PropertyDescriptor label="Required" value={required.includes(propertyName) ? 'true' : 'false'} />
                <PropertyDescriptor label="Description" value={property.description} />
                <PropertyDescriptor label="Values" value={property.enum ? `[ ${property.enum.join(', ')} ]` : null} />
                {property.type === 'object' ? <NestedProperty>{renderSchemaNode(property)}</NestedProperty> : null}
            </Property>
        );
    });
}

const getExtensionPointInfo = (name: Props['name'], schemas: Props['schemas']) => {
    if (!schemas) {
        return null;
    }

    const extensionsContent = schemas.schema ? renderSchemaNode(schemas.schema) : <div>No schema available for this extension</div>;
    const contextContent = schemas.contextSchema ? (
        renderSchemaNode(schemas.contextSchema)
    ) : (
        <div>No context available for this extension</div>
    );

    return (
        <InfoContainer>
            <ExtensionPointName>{name}</ExtensionPointName>
            <h2>Provided context</h2>
            {contextContent}
            <h2>Supported attributes</h2>
            {extensionsContent}
        </InfoContainer>
    );
};

const ExtensionPointInfo = (props: Props) => {
    const [showExtensionPointInfo] = useDiscovery();

    const { name, schemas } = props;
    const [dialogOpen, setDialogState] = useState(false);

    const content = getExtensionPointInfo(name, schemas);

    const closeDialog = () => setDialogState(false);
    return !showExtensionPointInfo ? null : (
        <BlinkerContainer>
            <Blinker onClick={() => setDialogState(true)} />
            <ModalTransition>
                {dialogOpen && (
                    <Modal actions={[{ text: 'Close', onClick: closeDialog }]} onClose={closeDialog} heading="Extension Point information">
                        {content}
                    </Modal>
                )}
            </ModalTransition>
        </BlinkerContainer>
    );
};

export default ExtensionPointInfo;
