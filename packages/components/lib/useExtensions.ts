import { useEffect, useMemo, useRef, useState } from 'react';
import isEqual from 'lodash.isequal';
import registry, {
    PluginAttributes,
    PluginAttributesProvider,
    PluginCleanupCallback,
    PluginDescriptor,
    PluginSchema,
} from '@atlassian/clientside-extensions-registry';
import { onDebug } from '@atlassian/clientside-extensions-debug';
import { JSONSchema7 } from 'json-schema';
import { validateObjectAgainstSchema } from './validation/validation';

type RawExtensionPointState = [PluginDescriptor[], boolean];
type ExtensionState<T> = [PluginDescriptor<T>[], PluginDescriptor[], boolean];
type ExtensionStateTouple = [PluginDescriptor[], PluginDescriptor[]];

const useExtensionPointSubscription = (extensionPoint: string): RawExtensionPointState => {
    const [extensionState, setExtensionState] = useState<RawExtensionPointState>([[], true]);

    useEffect(() => {
        const subscription = registry.getLocation(extensionPoint).subscribe(({ descriptors, loadingState }) => {
            setExtensionState([descriptors, loadingState]);
        });

        return () => subscription.unsubscribe();
    }, [extensionPoint]);

    return extensionState;
};

type ExtensionAttributeUpdateDiff = { [key: string]: PluginAttributes };
const updateDescriptors = (descriptors: PluginDescriptor[], diffs: ExtensionAttributeUpdateDiff): PluginDescriptor[] => {
    return descriptors.map(descriptor => {
        const maybeAttributes = diffs[descriptor.key];
        if (!maybeAttributes) {
            return descriptor;
        }

        return {
            ...descriptor,
            attributes: maybeAttributes,
        };
    });
};

export interface Options {
    schema: PluginSchema;
    contextSchema?: PluginSchema;
}

type AnimationQueue<T> = { [key: string]: T };
const useAnimationFrameQueue = <T>(callback: (params: AnimationQueue<T>) => void, deps: unknown[]) => {
    return useMemo(() => {
        let queue: AnimationQueue<T> = {};
        let requestIsQueued = false;
        const flushUpdate = () => {
            const queueClone = { ...queue };
            requestIsQueued = false;
            queue = {};
            callback(queueClone);
        };
        const requestUpdate = () => {
            if (!requestIsQueued) {
                requestIsQueued = true;
                requestAnimationFrame(flushUpdate);
            }
        };

        return (key: string, attributes: T) => {
            queue[key] = attributes;
            requestUpdate();
        };
    }, deps);
};

const useConditionalReference = <T>(value: T, cleanupFn: (param: T) => void, deps: unknown[]) => {
    const memoedValue = useMemo<T>(() => value, deps);
    useEffect(() => () => cleanupFn(memoedValue), deps);
    return memoedValue;
};

const safeguardAction = (action: (...args: unknown[]) => void | undefined, descriptorKey: string, location: string) => {
    if (!action) {
        return undefined;
    }

    return (...args: unknown[]) => {
        try {
            return action(...args);
        } catch (e) {
            onDebug(({ error }) => ({
                level: error,
                message: `Failed to execute onAction callback for plugin "${descriptorKey}" at extension: ${location}.
    Error: ${e}`,
                meta: {
                    plugin: descriptorKey,
                    location,
                },
            }));
            return undefined;
        }
    };
};

const mergeAttributes = (base: PluginAttributes, provided: PluginAttributes, onAction?: PluginAttributes['onAction']) => {
    if (!onAction) {
        return {
            ...base,
            ...provided,
        };
    }

    return {
        ...base,
        ...provided,
        onAction,
    };
};

const noopProvider: PluginAttributesProvider = () => ({});

const useDeepCompareMemoize = <T>(value: T): T => {
    const ref = useRef<T>({} as T);

    if (!isEqual(value, ref.current)) {
        ref.current = value;
    }

    return ref.current;
};

const validationErrorHanderFactory = (pluginKey: string, location: string, schema?: object, attributes?: object) => (e: Error): void => {
    onDebug(({ error }) => ({
        level: error,
        message: `The attributes provided for extension ${pluginKey} do not match the schema:
    ${e}`,
        components: ['Schema Validation'],
        meta: {
            location,
            plugin: pluginKey,
            schema,
            attributes,
            validation: e,
        },
    }));
    return undefined;
};

export const useExtensionsAll = <T = PluginAttributes>(name: string, context: object | null, options: Options): ExtensionState<T> => {
    const extensionSchema = options.schema as JSONSchema7;
    const contextSchema = options.contextSchema as JSONSchema7;

    if (!extensionSchema) {
        throw new Error(`No schema specified for extension point "${name}"`);
    }
    if (!contextSchema && context) {
        throw new Error(`No context schema specified for extension point "${name}"`);
    } else if (contextSchema) {
        validateObjectAgainstSchema(contextSchema, context).then(
            () => {
                // everything is fine
            },
            e => {
                onDebug(({ error }) => ({
                    level: error,
                    message: `The context provided for extension-location "${name}" does not match the schema:
    ${e}`,
                    components: ['Schema Validation'],
                    meta: {
                        name,
                        context,
                        validation: e,
                    },
                }));
            },
        );
    }

    const [rawDescriptors, loadingState] = useExtensionPointSubscription(name);
    const descriptorReference = useRef<PluginDescriptor[]>(null);
    const [descriptors, setDescriptors] = useState<PluginDescriptor[]>([]);

    descriptorReference.current = descriptors;

    const cachedContext = useDeepCompareMemoize(context);
    const keepInSyncDeps = [cachedContext, rawDescriptors];

    // queue that will only keep the last call to update in stock and apply on requestAnimationFrame
    const queue = useAnimationFrameQueue<PluginAttributes>(queue => {
        setDescriptors(updateDescriptors(descriptorReference.current, queue));
    }, keepInSyncDeps);

    // keep the reference of the value static as long as "deps" stay the same
    // allows to specify a callback to "clean up" reference once outdated.
    const cleanupCallbacks = useConditionalReference<[string, PluginCleanupCallback][]>(
        [],
        callbacks => {
            while (callbacks.length) {
                const [descriptorKey, callback] = callbacks.shift();
                try {
                    callback();
                } catch (e) {
                    onDebug(({ error }) => ({
                        level: error,
                        message: `Failed to execute cleanup callback registered for "${descriptorKey}".
See passed callback for reference: ${callback}`,
                        meta: {
                            plugin: descriptorKey,
                            callback,
                        },
                    }));
                }
            }
        },
        keepInSyncDeps,
    );

    useMemo(() => {
        const extensionDescriptors = rawDescriptors.map(
            ({ attributesProvider = noopProvider, attributes: baseAttributes, ...descriptor }) => {
                let providedAttributes = {} as PluginAttributes;
                try {
                    providedAttributes = attributesProvider(
                        {
                            updateAttributes(fnOrAttributes) {
                                const { key } = descriptor;
                                const currentDescriptor = descriptorReference.current.find(desc => desc.key === key);
                                const currentAttributes = currentDescriptor ? currentDescriptor.attributes : {};
                                try {
                                    const updatedAttributes =
                                        typeof fnOrAttributes === 'function' ? fnOrAttributes(currentAttributes) : fnOrAttributes;
                                    const attributes = { ...currentAttributes, ...updatedAttributes };
                                    validateObjectAgainstSchema(extensionSchema, attributes).then(() => {
                                        queue(descriptor.key, attributes);
                                    }, validationErrorHanderFactory(descriptor.key, name, extensionSchema, attributes));
                                } catch (e) {
                                    onDebug(({ error }) => ({
                                        level: error,
                                        message: `Updating attributes for plugin ${descriptor.key} failed:
    ${e}`,
                                        meta: {
                                            plugin: descriptor.key,
                                            location: name,
                                        },
                                    }));
                                }
                            },
                            onCleanup(cleanupCallback) {
                                cleanupCallbacks.push([descriptor.key, cleanupCallback]);
                            },
                        },
                        cachedContext,
                    );
                } catch (e) {
                    onDebug(({ error }) => ({
                        level: error,
                        message: `Calling the attributes provider for plugin ${descriptor.key} failed:
    ${e}`,
                        meta: {
                            plugin: descriptor.key,
                            location: name,
                        },
                    }));

                    return Promise.resolve(undefined);
                }
                const onAction = safeguardAction(providedAttributes.onAction, descriptor.key, name);

                const attributes = mergeAttributes(baseAttributes, providedAttributes, onAction);

                return validateObjectAgainstSchema(extensionSchema, attributes).then((): PluginDescriptor => {
                    return {
                        ...descriptor,
                        attributes,
                    };
                }, validationErrorHanderFactory(descriptor.key, name, extensionSchema, attributes));
            },
        );
        Promise.all(extensionDescriptors).then(resolvedDescriptors => {
            setDescriptors(resolvedDescriptors.filter(Boolean) as PluginDescriptor[]);
        });
    }, keepInSyncDeps);

    const [supportedDescriptors, unsupportedDescriptors] = useMemo<ExtensionStateTouple>(() => {
        return descriptors.reduce(
            (agg, descriptor) => {
                const { type } = descriptor.attributes;
                if (!type) {
                    onDebug(({ warn }) => ({
                        level: warn,
                        // eslint-disable-next-line max-len
                        message: `No handler type was specified for extension "${descriptor.key}" on extension point "${name}". Please check the documentation for accepted types.`,
                        meta: {
                            plugin: descriptor.key,
                            location: name,
                        },
                    }));
                    agg[1].push(descriptor);
                    return agg;
                }

                agg[0].push(descriptor);

                return agg;
            },
            [[], []] as ExtensionStateTouple,
        );
    }, [descriptors]);
    return [(supportedDescriptors as unknown) as PluginDescriptor<T>[], unsupportedDescriptors, loadingState];
};

export const useExtensions = <T = PluginAttributes>(name: string, context: object | null, options: Options) => {
    const extensionsPayload = useExtensionsAll<T>(name, context, options);
    return extensionsPayload[0];
};

export const useExtensionsUnsupported = <T = PluginAttributes>(name: string, context: object | null, options: Options) => {
    const extensionsPayload = useExtensionsAll<T>(name, context, options);
    return extensionsPayload[1];
};

export const useExtensionsLoadingState = <T = PluginAttributes>(name: string, context: object | null, options: Options) => {
    const extensionsPayload = useExtensionsAll<T>(name, context, options);
    return extensionsPayload[2];
};
