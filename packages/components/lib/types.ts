import { ReactNode } from 'react';

export type ExtensionPointCallback<T> = (supportedDescriptors: T[], unsupportedDescriptors: T[], loading: boolean) => ReactNode;
