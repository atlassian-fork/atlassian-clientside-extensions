// eslint-disable-next-line import/no-extraneous-dependencies
import { JSONSchema7 } from 'json-schema';
import { mocked } from 'ts-jest/utils';
import { isValidationEnabled } from '@atlassian/clientside-extensions-debug';

import { validateObjectAgainstSchema } from './validation';
import * as validateAsync from './validation-async';

jest.mock('@atlassian/clientside-extensions-debug', () => ({
    isValidationEnabled: jest.fn(() => true),
}));

const testSchema: JSONSchema7 = {
    type: 'object',
    properties: {
        type: {
            type: 'string',
            description: 'Web-item type',
            enum: ['modal', 'link', 'button', 'panel'],
        },
    },
    required: ['label'],
};

const testAttributes = {
    label: 'Fake label',
};

describe('listening to validation flag', () => {
    beforeEach(() => {
        mocked(isValidationEnabled).mockClear();
    });

    it('should only validate when validation is enabled', async () => {
        const validateSpy = jest.spyOn(validateAsync, 'validateSchema');
        mocked(isValidationEnabled).mockReturnValueOnce(false);

        await validateObjectAgainstSchema(testSchema, testAttributes);

        expect(validateSpy).not.toHaveBeenCalled();

        mocked(isValidationEnabled).mockReturnValueOnce(true);

        await validateObjectAgainstSchema(testSchema, testAttributes);

        expect(validateSpy).toHaveBeenCalledTimes(1);
    });
});

describe('returning validation result as promise', () => {
    it('should resolve the returning promise if validation pass', () => {
        return expect(validateObjectAgainstSchema(testSchema, testAttributes)).resolves.toBe(undefined);
    });

    it('should reject the returning promise if validation fails', () => {
        const wrongTestAttributes = {
            laaabel: 'Fake laaabel',
        };
        return expect(validateObjectAgainstSchema(testSchema, wrongTestAttributes)).rejects.toThrow();
    });
});
