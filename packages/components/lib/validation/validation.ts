// eslint-disable-next-line import/no-extraneous-dependencies
import { JSONSchema7 } from 'json-schema';
import { isValidationEnabled } from '@atlassian/clientside-extensions-debug';

// eslint-disable-next-line import/prefer-default-export
export const validateObjectAgainstSchema = (schema: JSONSchema7, objectUnderTest: object) => {
    return new Promise((resolve, reject) => {
        if (!isValidationEnabled()) {
            resolve();
            return;
        }

        import('./validation-async').then(({ validateSchema }) => {
            try {
                validateSchema(schema, objectUnderTest);
                resolve();
            } catch (e) {
                reject(e);
            }
        });
    });
};
