# Client-side Extensions

## Registry

Main registry for Plugins and Locations. This registry should be provided by Products as part of their superbatch.

Refer to the [official documentation](https://developer.atlassian.com/server/framework/clientside-extensions) for more information.
