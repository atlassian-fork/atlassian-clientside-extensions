export type Context<T extends object> = {
    [K in keyof T]: T[K];
};

interface PluginAction {
    onAction?: (...args: unknown[]) => void;
}

export interface PluginAttributes extends PluginAction {
    [key: string]: unknown;
}

interface BasePluginDescriptor<T> {
    key: string;
    location: string;
    weight: number;
    attributes: T;
    params?: {
        [key: string]: unknown;
    };
}

type AttributeUpdatePayload = PluginAttributes | ((previousAttributes: PluginAttributes) => PluginAttributes);
export type PluginCleanupCallback = () => void;
export interface PluginAPI {
    updateAttributes: (payload: AttributeUpdatePayload) => void;
    onCleanup: (callback: PluginCleanupCallback) => void;
}

export type PluginAttributesProvider = (api: PluginAPI, context: Context<{}>) => PluginAttributes;

export interface PluginDescriptor<T = PluginAttributes> extends BasePluginDescriptor<T> {
    attributesProvider?: PluginAttributesProvider;
}

/**
 * We need to keep using this type because TS can't assert JSON Schema literal types correctly.
 * https://github.com/microsoft/TypeScript/issues/31920
 * https://github.com/microsoft/TypeScript/issues/26552
 */
export type PluginSchema = object;

export interface WRMInterface {
    require(param: string | string[]): Promise<void>;
    contextPath(): string;
}
