import { PluginDescriptor, WRMInterface } from './types';

declare let WRM: WRMInterface;

function getClientPluginsRestURL(location: string) {
    const baseURL = `${WRM.contextPath()}/rest/client-plugins/1.0/client-plugins`;

    return `${baseURL}/items?location=${location}`;
}

export function getLocationPlugins(location: string): Promise<PluginDescriptor[]> {
    // TODO: should we use fetch? what's the standard way of doing Ajax requests in Server products?
    return fetch(getClientPluginsRestURL(location))
        .then(res => res.json())
        .then(res => res.webItems || []);
}

export function getLocationResources(location: string): Promise<void> {
    return WRM.require(`wrc!${location}`);
}
