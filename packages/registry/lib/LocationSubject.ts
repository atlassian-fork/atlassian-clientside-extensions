import { DeferredSubject } from '@atlassian/clientside-extensions-base';
import { PluginDescriptor } from './types';

type LocationsSubjectPayload = {
    descriptors: PluginDescriptor[];
    loadingState: boolean;
};

/**
 * LocationSubject used by the client plugin registry to keep track of plugin points
 */
export default class LocationsSubject extends DeferredSubject<LocationsSubjectPayload> {}
