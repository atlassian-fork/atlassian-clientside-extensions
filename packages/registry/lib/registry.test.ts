import { onDebug } from '@atlassian/clientside-extensions-debug';
import { mocked } from 'ts-jest/utils';

import LocationSubject from './LocationSubject';
import ClientPluginsRegistry from './registry';
import { getLocationPlugins, getLocationResources } from './httpUtils';
import { PluginDescriptor, PluginAttributesProvider } from './types';

jest.mock('@atlassian/clientside-extensions-debug');
jest.mock('./httpUtils');

type LocationSubjectObserver = Parameters<LocationSubject['subscribe']>[0];

// Needed in order to resolve/reject promises outside their executor
class Deferred<T> {
    private resolveCallback: (value: T) => void;

    private rejectCallback: (reason: unknown) => void;

    private deferredPromise: Promise<T> = new Promise<T>((resolve, reject) => {
        this.rejectCallback = reject;
        this.resolveCallback = resolve;
    });

    public get promise(): Promise<T> {
        return this.deferredPromise;
    }

    public resolve(value: T) {
        this.resolveCallback(value);
    }

    public reject(reason?: unknown) {
        this.rejectCallback(reason);
    }
}

/**
 * Wrapping all tests in promises to avoid jest/no-test-callback
 * https://github.com/jest-community/eslint-plugin-jest/blob/master/docs/rules/no-test-callback.md
 */

describe('registry', () => {
    describe('registry location management', () => {
        const testDescriptors: PluginDescriptor[] = [
            { key: 'test-plugin-1', location: 'test-location', weight: 10, attributes: {} },
            { key: 'test-plugin-2', location: 'test-location', weight: 10, attributes: {} },
        ];
        let registry: ClientPluginsRegistry;
        let deferredPlugins: Deferred<PluginDescriptor[]>;

        beforeEach(() => {
            registry = new ClientPluginsRegistry();
            deferredPlugins = new Deferred<PluginDescriptor[]>();

            jest.resetAllMocks();

            mocked(getLocationPlugins).mockImplementation(() => deferredPlugins.promise);
            mocked(getLocationResources).mockImplementation(() => Promise.resolve());
        });

        it('should warn and return empty list when trying to get a location with an empty name', () => {
            expect.assertions(3);
            return new Promise(done => {
                const invalidLocationCase: LocationSubjectObserver = ({ descriptors, loadingState }) => {
                    expect(onDebug).toHaveBeenCalled();
                    expect(descriptors).toEqual([]);
                    expect(loadingState).toBe(false);

                    done();
                };

                const cases = [invalidLocationCase];

                registry.getLocation(undefined).subscribe(payload => {
                    cases.shift()(payload);
                });
            });
        });

        it('should be in loading state while it loads the descriptors and resources', () => {
            expect.assertions(2);
            return new Promise(done => {
                const loadingResolveCase: LocationSubjectObserver = ({ loadingState }) => {
                    expect(loadingState).toBe(true);

                    deferredPlugins.resolve([]);
                };

                const loadingFinishedCase: LocationSubjectObserver = ({ loadingState }) => {
                    expect(loadingState).toBe(false);

                    done();
                };

                const cases = [loadingResolveCase, loadingFinishedCase];

                registry.getLocation('test-location').subscribe(payload => {
                    cases.shift()(payload);
                });
            });
        });

        it('should notify locations with the list of descriptors even when none of them have resources (legacy web-items)', () => {
            expect.assertions(3);
            return new Promise(done => {
                const loadingResolveCase: LocationSubjectObserver = ({ loadingState }) => {
                    expect(loadingState).toBe(true);
                    deferredPlugins.resolve(testDescriptors);
                };

                const descriptorsWithoutResourcesCase: LocationSubjectObserver = ({ descriptors, loadingState }) => {
                    expect(descriptors).toMatchObject(testDescriptors);
                    expect(loadingState).toBe(false);

                    done();
                };

                const cases = [loadingResolveCase, descriptorsWithoutResourcesCase];

                registry.getLocation('test-location').subscribe(payload => {
                    cases.shift()(payload);
                });
            });
        });

        it('should warn but not explode if something went wrong getting the plugin descriptors/resources', () => {
            expect.assertions(4);
            return new Promise(done => {
                const loadingRejectCase: LocationSubjectObserver = ({ loadingState }) => {
                    expect(loadingState).toBe(true);

                    deferredPlugins.reject();
                };

                const warnCase: LocationSubjectObserver = ({ descriptors, loadingState }) => {
                    expect(onDebug).toHaveBeenCalled();
                    expect(descriptors).toEqual([]);
                    expect(loadingState).toBe(false);

                    done();
                };

                const cases = [loadingRejectCase, warnCase];

                registry.getLocation('test-location').subscribe(payload => {
                    cases.shift()(payload);
                });
            });
        });

        it('should only initialize a location once', () => {
            expect.assertions(10);
            return new Promise(done => {
                const loadingCase: LocationSubjectObserver = ({ loadingState }) => {
                    expect(loadingState).toBe(true);
                };

                const loadingResolveCase: LocationSubjectObserver = payload => {
                    loadingCase(payload);

                    deferredPlugins.resolve(testDescriptors);
                };

                const receiveDescriptorsCase: LocationSubjectObserver = ({ descriptors, loadingState }) => {
                    expect(loadingState).toBe(false);
                    expect(descriptors).toMatchObject(testDescriptors);
                    expect(getLocationPlugins).toHaveBeenCalledTimes(1);
                    expect(getLocationResources).toHaveBeenCalledTimes(1);
                };

                const receiveDescriptorsDoneCase: LocationSubjectObserver = payload => {
                    receiveDescriptorsCase(payload);

                    done();
                };

                const observer1Cases = [loadingCase, receiveDescriptorsCase];
                registry.getLocation('test-location').subscribe(payload => {
                    observer1Cases.shift()(payload);
                });

                // Observer asserts the same, but is the one responsible for executing mock side effects
                const observer2Cases = [loadingResolveCase, receiveDescriptorsDoneCase];
                registry.getLocation('test-location').subscribe(payload => {
                    observer2Cases.shift()(payload);
                });
            });
        });
    });

    describe('registry entry-points management', () => {
        const testDescriptors: PluginDescriptor[] = [
            { key: 'test-plugin-1', location: 'test-location', weight: 10, attributes: {} },
            { key: 'test-plugin-2', location: 'test-location', weight: 10, attributes: {} },
        ];
        let registry: ClientPluginsRegistry;
        let deferredResources: Deferred<void>;

        beforeEach(() => {
            registry = new ClientPluginsRegistry();
            deferredResources = new Deferred<void>();

            jest.resetAllMocks();

            mocked(getLocationPlugins).mockImplementation(() => Promise.resolve(testDescriptors));
            mocked(getLocationResources).mockImplementation(() => deferredResources.promise);
        });

        it('should warn if trying to register an attribute provider for a plugin that does not exists', () => {
            expect.assertions(4);
            return new Promise(done => {
                const loadingCase: LocationSubjectObserver = ({ loadingState }) => {
                    expect(loadingState).toBe(true);
                    registry.registerExtension('test-fail-plugin', () => ({ type: 'link' }));
                    deferredResources.resolve();
                };

                const registerExtensionWarnCase: LocationSubjectObserver = ({ descriptors, loadingState }) => {
                    expect(loadingState).toBe(false);
                    expect(descriptors).toMatchObject(testDescriptors);
                    expect(onDebug).toHaveBeenCalled();

                    done();
                };

                const cases = [loadingCase, registerExtensionWarnCase];

                registry.getLocation('test-location').subscribe(payload => {
                    cases.shift()(payload);
                });
            });
        });

        it('should add the attributes provider to the plugin descriptor', () => {
            expect.assertions(4);
            return new Promise(done => {
                const attributesProvider: PluginAttributesProvider = () => ({
                    type: 'link',
                    link: 'http://fake-link',
                });

                const loadingCase: LocationSubjectObserver = ({ loadingState }) => {
                    expect(loadingState).toBe(true);
                    registry.registerExtension('test-plugin-1', attributesProvider);
                    deferredResources.resolve();
                };

                const registerExtensionWarnCase: LocationSubjectObserver = ({ descriptors, loadingState }) => {
                    const [firstDescriptor] = descriptors;
                    const [firstTestDescriptor, secondTestDescriptor] = testDescriptors;

                    expect(loadingState).toBe(false);
                    expect(descriptors).toMatchObject([{ ...firstTestDescriptor, attributesProvider }, secondTestDescriptor]);
                    expect(firstDescriptor.attributesProvider).toBe(attributesProvider);

                    done();
                };

                const cases = [loadingCase, registerExtensionWarnCase];

                registry.getLocation('test-location').subscribe(payload => {
                    cases.shift()(payload);
                });
            });
        });
    });
});
