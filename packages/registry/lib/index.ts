import ClientPluginsRegistry from './registry';

declare global {
    interface Window {
        ____c_p_r: ClientPluginsRegistry;
    }
}

// Create a singleton instance for the default export and ensure it can only ever exist once by sharing it globally
// eslint-disable-next-line no-multi-assign,no-underscore-dangle
const registry = (window.____c_p_r = window.____c_p_r || new ClientPluginsRegistry());

export default registry;
export * from './types';
