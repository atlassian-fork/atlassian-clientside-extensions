import ReplaySubject from './ReplaySubject';

describe('ReplaySubject', () => {
    let subject: ReplaySubject<number>;

    beforeEach(() => {
        subject = new ReplaySubject();
    });

    it('should regularly notify observer with payload', () => {
        // we expect 2 assertions to happen
        expect.assertions(1);

        const TEST_PAYLOAD = 1;

        subject.subscribe(payload => {
            expect(payload).toBe(TEST_PAYLOAD);
        });

        subject.notify(TEST_PAYLOAD);
    });

    it('should replay previous notifications to new subscribers', () => {
        expect.assertions(10);
        const ALL_PAYLOADS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        ALL_PAYLOADS.forEach(payload => {
            subject.notify(payload);
        });

        let count = 0;
        subject.subscribe(payload => {
            expect(payload).toBe(ALL_PAYLOADS[count]);
            count += 1;
        });
    });

    it('should accept and respect replay length on construction', () => {
        subject = new ReplaySubject(5);
        const ALL_PAYLOADS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        expect.assertions(5);

        ALL_PAYLOADS.forEach(payload => {
            subject.notify(payload);
        });

        let count = 5;
        subject.subscribe(payload => {
            expect(payload).toBe(ALL_PAYLOADS[count]);
            count += 1;
        });
    });
});
