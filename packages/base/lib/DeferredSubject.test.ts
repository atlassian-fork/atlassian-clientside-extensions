import DeferredSubject from './DeferredSubject';

jest.useFakeTimers();

describe('DeferredSubject', () => {
    let subject: DeferredSubject<boolean>;

    beforeEach(() => {
        subject = new DeferredSubject();
    });

    it('should defer notifying each observer', () => {
        const TEST_PAYLOAD = true;

        const observer1 = jest.fn();
        const observer2 = jest.fn();
        subject.subscribe(observer1);
        subject.subscribe(observer2);

        subject.notify(TEST_PAYLOAD);
        expect(observer1).not.toHaveBeenCalled();
        expect(observer2).not.toHaveBeenCalled();

        jest.runAllTimers();
        expect(observer1).toHaveBeenCalledWith(TEST_PAYLOAD);
        expect(observer2).toHaveBeenCalledWith(TEST_PAYLOAD);
    });

    it('should defer notifying observers when calling notify consecutive times', () => {
        const TEST_PAYLOAD = true;

        const observer1 = jest.fn();
        subject.subscribe(observer1);

        subject.notify(TEST_PAYLOAD);
        expect(observer1).not.toHaveBeenCalled();

        jest.runAllTimers();
        expect(observer1).toHaveBeenCalledTimes(1);

        // "random" amount of immediate subsequent calls
        subject.notify(TEST_PAYLOAD);
        subject.notify(TEST_PAYLOAD);
        subject.notify(TEST_PAYLOAD);
        subject.notify(TEST_PAYLOAD);
        subject.notify(TEST_PAYLOAD);

        jest.runAllTimers();
        expect(observer1).toHaveBeenCalledTimes(2);
    });

    it('should immediately call late subscribers with the last payload if no timer is running', () => {
        const TEST_PAYLOAD = false;

        const preNotifyObserver = jest.fn();
        const postNotifyObserver = jest.fn();

        // no notifications yet
        subject.subscribe(preNotifyObserver);

        // we register and start the deferred timer
        subject.notify(TEST_PAYLOAD);
        // in the loop waiting to be called
        expect(preNotifyObserver).not.toHaveBeenCalled();

        // run timer
        jest.runAllTimers();
        // should be called now - no more "waiting" happening
        expect(preNotifyObserver).toHaveBeenCalled();

        // should be called immediatelly as no timer is running.
        subject.subscribe(postNotifyObserver);

        expect(postNotifyObserver).toHaveBeenCalled();
    });

    it('should call late subscribers within the same notification loop as other observers if a timer is running', () => {
        const TEST_PAYLOAD = false;

        const preNotifyObserver = jest.fn();
        const postNotifyObserver = jest.fn();

        // no notifications yet
        subject.subscribe(preNotifyObserver);

        // we register and start the deferred timer
        subject.notify(TEST_PAYLOAD);
        // in the loop waiting to be called
        expect(preNotifyObserver).not.toHaveBeenCalled();

        // add late subscriber
        subject.subscribe(postNotifyObserver);
        // should not be called as a timer is running
        expect(postNotifyObserver).not.toHaveBeenCalled();

        // run timer
        jest.runAllTimers();
        expect(preNotifyObserver).toHaveBeenCalled();
        expect(postNotifyObserver).toHaveBeenCalled();
    });

    it('should not call subscribers that deregister before the timer has finished', () => {
        const TEST_PAYLOAD = false;

        const shouldRun = jest.fn();
        const deregistersBeforeRunning = jest.fn();

        // no notifications yet
        subject.subscribe(shouldRun);
        const subscription = subject.subscribe(deregistersBeforeRunning);

        subject.notify(TEST_PAYLOAD);
        expect(shouldRun).not.toHaveBeenCalled();
        expect(deregistersBeforeRunning).not.toHaveBeenCalled();
        // unregister the `deregistersBeforeRunning`
        subscription.unsubscribe();

        jest.runAllTimers();

        expect(shouldRun).toHaveBeenCalled();
        expect(deregistersBeforeRunning).not.toHaveBeenCalled();
    });
});
