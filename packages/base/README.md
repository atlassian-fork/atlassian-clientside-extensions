# Client-side Extensions

## Common Internal components

Base components used internally shared across the other packages for the client-side extensions.

Refer to the [official documentation](https://developer.atlassian.com/server/framework/clientside-extensions) for more information.
