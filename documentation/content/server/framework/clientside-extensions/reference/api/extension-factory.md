---
title: Extension Factory - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: api
date: '2020-02-19'
---

# Extension Factory

## About

An extension factory is a set of provided helper utilities to create [extensions](/server/framework/clientside-extensions/reference/glossary/).

They will set the type of the extensions automatically, and provide type definitions for your [attribute provider](/server/framework/clientside-extensions/reference/glossary/) if you're using TypeScript.

### Signature

```ts
type factory = (api: ExtensionAPI, context: Context<{}>) => ExtensionAttributes;
```

### Parameters

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>extensionAPI</td>
            <td><code>ExtensionAPI</code></td>
            <td><a href="/server/framework/clientside-extensions/reference/api/extension-api">Extension API</a> that provides methods to render and clean up the extension.</td>
        </tr>
        <tr>
            <td>context</td>
            <td><code>object</code></td>
            <td>An object with information that products share with extensions to share important information about the screen where they are rendered.</td>
        </tr>
    </tbody>
</table>

### Custom types and attributes

There is a set of default attributes that all products support, but they can also add custom attributes or extension types to extend their UI in custom ways.

Check the documentation of each product's extension point for more details.

## Link

A link extension allows to render a link on the screen and navigate to another page when clicked by a user.

### Supported attributes

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>label*</td>
            <td><code>string</code></td>
            <td>Text to be used as the label of the link.</td>
        </tr>
        <tr>
            <td>url*</td>
            <td><code>string</code></td>
            <td>Defines where the link should redirect the user to.</td>
        </tr>
        <tr>
            <td>isHidden</td>
            <td><code>boolean</code></td>
            <td>If true, the link won't be rendered. Keep in mind that the code of the factory will still be executed.</td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

### Usage

```ts
import { LinkExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 */
export default LinkExtension.factory((extensionAPI, context) => ({
    label: 'Go to DAC',
    url: 'https://developer.atlassian.com/',
}));
```

## Button

A button extension allows to render a button on the screen and execute an action when its clicked by a user.

### Supported attributes

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>label*</td>
            <td><code>string</code></td>
            <td>Text to be used as the label of the button.</td>
        </tr>
        <tr>
            <td>onAction*</td>
            <td><code>function</code></td>
            <td>
                <p><strong>signature: </strong> <code>() => void</code></p>
                <p>Method to bind as the click handler of the button.</p>
            </td>
        </tr>
        <tr>
            <td>isDisabled</td>
            <td><code>boolean</code></td>
            <td>If true, sets the button as disabled.</td>
        </tr>
        <tr>
            <td>isLoading</td>
            <td><code>boolean</code></td>
            <td>If true, renders the button in loading state.</td>
        </tr>
        <tr>
            <td>isHidden</td>
            <td><code>boolean</code></td>
            <td>If true, the button won't be rendered. Keep in mind that the code of the factory will still be executed.</td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

### Usage

```ts
import { ButtonExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 */
export default ButtonExtension.factory((extensionAPI, context) => {
    return {
        label: 'My Button',
        onAction: () => {
            // execute some action when clicked
        },
        // ... set isDisabled or isLoading if needed
    };
});
```

## Panel

A panel extension allows the creation of custom HTML content in a container provided by the plugin system.

### Supported attributes

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>label*</td>
            <td><code>string</code></td>
            <td>Optionally rendered by the product to describe the panel content. e.g. it may be the label of a tab in a list of tabs, or the text for a header above the panel HTML.</td>
        </tr>
        <tr>
            <td>onAction*</td>
            <td><code>function</code></td>
            <td>
                <p><strong>signature: </strong> <code>(panelAPI) => void</code></p>
                <p>Method to be called when the product is ready to render the panel. The plugin system will provide an API with lifecycles to render/clean up any current content into a provided container.</p>
                <p>Refer to <a href="/server/framework/clientside-extensions/reference/api/panel-api">Panel API</a> documentation for more info.</p>
            </td>
        </tr>
        <tr>
            <td>isHidden</td>
            <td><code>boolean</code></td>
            <td>If true, the panel won't be rendered. Keep in mind that the code of the factory will still be executed.</td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

### Usage

```ts
import { PanelExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 */
export default PanelExtension.factory(() => {
    return {
        label: 'JS Panel',
        onAction: panelApi => {
            panelApi
                .onMount(container => {
                    // use the container to render your content in it
                })
                .onUnmount(container => {
                    // run your clean up code. e.g. stop listening to events, unmount your component from the container.
                });
        },
    };
});
```

## Modal

A modal extension renders a button that opens a modal, and allows to specify a custom HTML content to the body of it.
It also provides a set of APIs to interact with this modal.

### Supported attributes

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>label*</td>
            <td><code>string</code></td>
            <td>Text to be used as the label of the link.</td>
        </tr>
        <tr>
            <td>onAction*</td>
            <td><code>function</code></td>
            <td>
                <p><strong>signature: </strong> <code>(modalAPI) => void</code></p>
                <p>Method to be called when the product is ready to render the Modal. The plugin system will provide an API with methods
                to modify the title, content and actions of the modal.</p>
                <p>Refer to <a href="/server/framework/clientside-extensions/reference/api/modal-api">Modal API</a> documentation for more info.</p>
            </td>
        </tr>
        <tr>
            <td>isDisabled</td>
            <td><code>boolean</code></td>
            <td>If true, sets the button that triggers the modal as disabled.</td>
        </tr>
        <tr>
            <td>isLoading</td>
            <td><code>boolean</code></td>
            <td>If true, renders the button that triggers the modal in loading state.</td>
        </tr>
        <tr>
            <td>isHidden</td>
            <td><code>boolean</code></td>
            <td>If true, the button that triggers the modal won't be rendered. Keep in mind that the code of the factory will still be executed.</td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

### Usage

```ts
import { ModalExtension } from '@atlassian/clientside-extensions';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 */
export default ModalExtension.factory((extensionAPI, context) => {
    return {
        label: 'Open Modal',
        onAction(modalAPI) {
            modalAPI.setTitle('A cool modal with JS');

            modalAPI.onMount((container, modalOptions) => {
                // use the container to render your content in it...
                // use the modalOptions to interact with the modal after opened...
            });

            modalAPI.onUnmount(() => {
                // run your cleanup code here...
            });
        },
    };
});
```
