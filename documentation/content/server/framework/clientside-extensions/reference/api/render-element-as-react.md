---
title: Render element as React - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: api
date: '2020-03-06'
---

# Render element as React

`renderElementAsReact` is an utility provided to render custom content using React with the Client-side Extensions API.

It handles the mount/unmount cycles and rendering/unmounting the provided component for you.
Also, it's recommended to use it since optimizations to React rendering content will be applied here in the future.

## Signature

```ts
type renderElementAsReact = <T>(
    renderApi: PanelAPI | ModalAPI,
    RenderElement: ComponentType<{ options: unknown } & Omit<T, 'options'>>,
    additionalProps?: Omit<T, 'options'>,
) => void;
```

## Arguments

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>renderApi*</td>
            <td><code>PanelAPI | ModalAPI</code></td>
            <td>The API object received in the <code>onAction</code> method should be provided as the argument. Only works for Panels and Modals.</td>
        </tr>
        <tr>
            <td>RenderElement*</td>
            <td><code>ComponentType</code></td>
            <td>A React Component.</td>
        </tr>
        <tr>
            <td>additionalProps</td>
            <td><code>object</code></td>
            <td>
                An optional object with additional props that will be bind to the component once mounted.
            </td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

## Usage notes

### 1. Rendering a react component as content of a panel

```ts
import { PanelExtension, renderElementAsReact } from '@atlassian/clientside-extensions';

import PanelContent from './panel-content';

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 */
export default PanelExtension.factory(() => {
    return {
        label: `React panel`,
        onAction(panelAPI) {
            renderElementAsReact(panelAPI, PanelContent);
        },
    };
});
```

### 2. Rendering a react component as content of a modal with additional props

```ts
import { ModalExtension, renderElementAsReact } from '@atlassian/clientside-extensions';
import React from 'react';

interface Context {
    /*...*/
}

type ReactComponentProps = {
    context: Context;
    modalAPI: ModalExtension.Api;
};

const ReactComponent = ({ context, modalAPI }: ReactComponentProps) => {
    modalAPI.setTitle('An awesome modal with react');

    modalAPI.onClose(() => {
        /*...*/
    });

    modalAPI.setActions([
        /*...*/
    ]);

    return ( /*..Your custom modal content goes here..*/);
};

/**
 * @clientside-extension
 * @extension-point reff.plugins-example-location
 */
export default ModalExtension.factory((api, context) => {
    return {
        label: `Modal with react content`,
        onAction(modalAPI) {
            renderElementAsReact<ReactComponentProps>(modalAPI, ReactComponent, { modalAPI, context });
        },
    };
});
```
