---
title: Webpack Plugin - Client-side Extensions
platform: server
product: clientside-extensions
category: reference
subcategory: webpack
date: '2020-01-07'
---

# Webpack plugin

Client-side Extensions webpack plugin depends on [WRM webpack plugin](https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin/src).

## Configuration

<table>
    <thead>
        <tr class="header">
            <th>Name</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>pattern*</td>
            <td>Glob pattern to specify which files should be concider as entry-points.</td>
        </tr>
        <tr>
            <td>cwd</td>
            <td>Configure a working directory to look for entry-points</td>
        </tr>
    </tbody>
</table>

<p><strong>* required</strong></p>

## generateEntrypoints()

Generates all the entry-point definitions for the webpack `entry` configuration.

## Usage

```js
const path = require('path');
const ClientsideExtensionsWebpackPlugin = require('@atlassian/clientside-extensions-webpack-plugin');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');

const wrmPlugin = new WrmPlugin(/*...*/);

const clientsideExtensions = new ClientsideExtensionsWebpackPlugin({
    pattern: '**/clientside-extensions/**/*.tsx',
    cwd: path.join(__dirname, './src/main/frontend'),
});

module.exports = {
    entry: {
        ...clientsideExtensions.generateEntrypoints(),
    },
    plugins: [wrmPlugin, clientsideExtensions],
    /*...*/
};
```
