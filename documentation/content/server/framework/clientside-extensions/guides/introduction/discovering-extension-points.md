---
title: Disconvering Extension Points - Client-side Extensions
platform: server
product: clientside-extensions
category: devguide
subcategory: introduction
date: '2020-03-02'
---

# Discovering extension points

{{% note %}}
This is a continuation of the work started in [1. Creating an extension](/server/framework/clientside-extensions/guides/introduction/creating-an-extension/).
{{% /note %}}

In this introduction guide, you will learn how to:

-   Reveal all the extension points in a page.
-   Discover all the possible types of extensions you can render in an extension point.
-   Check all the attributes and values you can use to influence how the product renders your extensions.

## Revealing extension points in the page

Client-side Extensions (CSE) comes with some developer tools baked in to help you reveal all extensions points in the page.

To activate it, you need to go to a page that you know is using CSE and set the query parameter `?clientside-extensions` in the URL.

1. Navigate to the PR you created for testing CSE (e.g: http://localhost:7990/bitbucket/projects/PROJECT_1/repos/rep_1/pull-requests/1/)
2. Add this query parameter to the URL: `?clientside-extensions`.

You should see a glowing blue circle icon in the places where extension points are available.

![Pull Request screenshot with discovery extension points revealed](/server/framework/clientside-extensions/images/revealing-extension-points.png)

## Exploring extension points information

After revealing the available extension points in a page, you can click on any of the glowing icons to discover all the
relevant information of a particular extension point:

1. Click the glowing icon in the overview actions section of the PR page.

You should be able to see:

-   The name of the extension point.
-   The type of extensions you can create for that extension point.
-   The attributes that are supported, and how they affect the way the product will render your extension.
-   The shape of the context object your extension will receive from the product.

{{% note %}}
In the next section, you will learn all about Context.
{{% /note %}}

![Pull Request screenshot with extension point information dialog](/server/framework/clientside-extensions/images/discovering-extension-points-information.png)

## Recap and next steps

Now you know:

-   How to reveal extension points in a page.
-   How to discover all extension types, supported attributes, and context provided for your extension point.

Next, you’re going to lear how to interact with products [using the extensions API](/server/framework/clientside-extensions/guides/introduction/using-extension-api)
