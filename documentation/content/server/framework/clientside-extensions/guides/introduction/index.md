---
title: Getting Started - Client-side Extensions
platform: server
product: clientside-extensions
category: devguide
subcategory: introduction
date: '2020-02-25'
---

# Getting started

For this introduction, you will be creating a set of extensions for the pull request page in Bitbucket Server 7.0, and explore
all the capabilities that Client-side Extensions brings to you as a front-end developer creating Atlassian Plugins.

The Bitbucket Server team provides a template to create a Bitbucket Plugin with all the tools already configured for you, including Client-side Extensions (CSE).

You can grab a copy by cloning [Bitbucket CSE template](https://bitbucket.org/atlassianlabs/bitbucket-client-side-extensions-template):

```
git clone git@bitbucket.org:atlassianlabs/bitbucket-client-side-extensions-template.git
```

## Installing the requirements

You will need to install:

-   Node 12.13.0 (you can use nvm)
-   Maven 3.6.2
-   Java JDK 1.8
-   [Atlassian SDK 8](https://developer.atlassian.com/server/framework/atlassian-sdk/downloads/)

## Starting Bitbucket

To install all the dependencies for the first time, run:

```
atlas-package -DskipTests
```

To start Bitbucket from your project, run:

```
atlas-run -DskipTests
```

## Developing

The template comes with watchmode and hot reload configured for your front-end code. You can run the CSE watch server executing:

```
npm start
```

## Creating a pull request to test your extensions

You will need to open a pull request in your local copy of Bitbucket.

The Bitbucket CSE template provides a testing project called `Project 1`, and inside, a testing repository called `rep_1`.

Let's create a PR in that repository:

1. Go to the Pull Request page for that repository: http://localhost:7990/bitbucket/projects/PROJECT_1/repos/rep_1/pull-requests
2. Click the **Create Pull Request** button:
3. Create a Pull Request using the branch **basic_branching**:

## Creating a working directory for your extensions

Now, create a folder to use as your working directory and store the code for your extensions.

Open the Bitbucket CSE template you cloned, and:

1. Create a folder called `getting-started` under `/src/my-app/extensions`. This will be the directory where you will be creating your extensions.

## You're ready!

That's it! You're now ready to [create your first extension](/server/framework/clientside-extensions/guides/introduction/creating-an-extension/)!
