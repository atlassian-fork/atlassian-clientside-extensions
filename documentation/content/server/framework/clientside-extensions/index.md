---
title: About Client-side Extensions
platform: server
product: clientside-extensions
category: devguide
subcategory: index
date: '2020-02-25'
---

# About Client-side Extensions

Client-side Extensions (CSE) is a set of APIs that allow a plugin author to augment and extend an Atlassian product's runtime using
client-side JavaScript code.

{{% note %}}
Client-side Extensions is a recent addition to Server Platform, so availability is still limited to the following products:

-   Bitbucket Server 7.0 - Pull Request experience

We're working hard with other products to provide CSE capabilities in the near future.
{{% /note %}}

## Getting started

If you're new to CSE, we recommend going through our [introduction guides](/server/framework/clientside-extensions/guides/introduction/).

You will be creating a set of extensions for the new Bitbucket PR experience, while learning about all the
capabilities that CSE brings to front-end developers creating Atlassian Plugins.

## Setup

In case you have an existing project, you can follow our guides on how to [setup CSE webpack plugin](/server/framework/clientside-extensions/guides/how-to/setup-webpack-plugin/)
and browse our [API Reference](/server/framework/clientside-extensions/reference/api/extension-factory) for a quick
look of all you can do with CSE.

## Not using webpack

If you're not using webpack, you can still make use of CSE in Bitbucket Server 7.0.
For examples on how to do so, explore [our demos](https://bitbucket.org/atlassian/atlassian-clientside-extensions/src/master/environments/refapp/src/main/resources/).
