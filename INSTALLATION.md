# Installing Client-side Extensions in your product

First, you need to install all the required artifacts and packages:

## Required maven artifacts

### atlassian-plugins-webresource

Provides ability to push CSS, JS, and other non-HTML assets in to the browser.

-   Version: `>= 4.0.0`
-   Status: `stable and public`
-   Code: [atlassian/atlassian-plugins-webresource](https://bitbucket.org/atlassian/atlassian-plugins-webresource)

### atlassian-plugins-webfragment

Provides ability to render 3rd-party HTML fragments inside 1st-party product views.

-   Version: `>= 5.0.0`
-   Status: `stable and public`
-   Code: [atlassian/atlassian-plugins-webfragment](https://bitbucket.org/atlassian/atlassian-plugins-webfragment/src/master/)

### atlassian-clientside-extensions-runtime

Provides browser runtime for registering and rendering web-fragments and web-resources client-side within the browser, as opposed to on the server-side.

-   Version: `>=0.6.0`
-   Status: `in development and published internally`
-   Code: [atlassian/atlassian-clientside-extensions](https://bitbucket.org/atlassian/atlassian-clientside-extensions/src/master/)
-   Artifact: [com/atlassian/plugins/atlassian-clientside-extensions-runtime/](https://packages.atlassian.com/webapp/#/artifacts/browse/tree/General/maven-private-local/com/atlassian/plugins/atlassian-clientside-extensions-runtime)

## Required Node packages

### @atlassian/clientside-extensions-components

Provides React components for registering extension-points in JS-rendered views.

-   Version: `>=0.6.0`
-   Status: `in development and published internally`
-   Code: [atlassian/atlassian-clientside-extensions](https://bitbucket.org/atlassian/atlassian-clientside-extensions/src/master/)
-   Package: [@atlassian/clientside-extensions-components](https://npmjs.com/package/@atlassian/clientside-extensions-components)

## Installation Process

### 1. Bundle the new platform pieces

Add all the maven projects listed above to your product’s webapp. Depending on how your product is set up, you will either want to:

-   Add them as a direct dependency of your webapp, or
-   If your product has a “bundled plugins” maven project, add them as a dependency to that.

#### Upgrading to webfragments v5

Follow the [upgrade guides](https://bitbucket.org/atlassian/atlassian-plugins-webfragment/src/master/) available in the Webfragments repo.

Ensure your product provides an implementation of `DynamicWebInterfaceManager`.

Ensure you provide an implementation of `WebItemModuleDescriptor` that can resolve the new `getEntryPoint` method.

#### Upgrading to webresources v4

Follow the [upgrade guides](https://bitbucket.org/atlassian/atlassian-plugins-webresource/) available in the WRM repo.

#### Bundle atlassian-plugin-point-safety

This is required to make the REST endpoint of our runtime work.

### 2. Bundle the new runtime to the product

You need to ensure the client plugins registry is loaded synchronously along with the product’s other runtime code. This likely means adding the web-resource key for the client-side registry to your product’s superbatch.

The full webresource key for the registry is:

```
com.atlassian.plugins.atlassian-clientside-extensions-runtime:runtime
```

### 3. Add @atlassian/clientside-extensions-registry as a provided dependency

As at `31 Oct 2019`, the React components will pull in a redundant copy of the runtime. This can be deduplicated via Webpack + the WRM plugin by adding a providedDependency for the runtime:

```js
new WRMPlugin({
    providedDependencies: {
        '@atlassian/clientside-extensions-registry': {
            dependency: 'com.atlassian.plugins.atlassian-clientside-extensions-runtime:runtime',
            import: {
                var: "require('@atlassian/clientside-extensions-registry')",
                amd: '@atlassian/clientside-extensions-registry',
            },
        },
    },
});
```
