package com.atlassian.rest.plugins;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.anyMap;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ClientsideExtensionsResourceTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private ClientsideExtensionsResource resource;

    @Mock
    private DynamicWebInterfaceManager dynamicWebInterfaceManager;

    @Test
    public void testGetWebItemsWhenLocationExistsWithAttributeShouldReturnWebItemWithGivenAttribute() {
        // Arrange
        WebItem webItem = mock(WebItem.class);
        when(webItem.getWeight()).thenReturn(100);
        when(dynamicWebInterfaceManager.getDisplayableWebItems(anyString(), anyMap())).thenReturn(singletonList(webItem));
        // Act
        Response response = resource.getWebItems("mylocation");
        ClientsideExtensionsAssetsBean bean = (ClientsideExtensionsAssetsBean) response.getEntity();
        // Assert
        assertThat(bean.getWebItems().get(0).getWeight(), equalTo(100));
    }

    @Test
    public void testGetWebItemsWithExistingWebItemShouldHaveAWebItemInstance() {
        // Arrange
        WebItem webItem = mock(WebItem.class);
        when(dynamicWebInterfaceManager.getDisplayableWebItems(anyString(), anyMap())).thenReturn(singletonList(webItem));
        // Act
        Response response = resource.getWebItems("mylocation");
        ClientsideExtensionsAssetsBean bean = (ClientsideExtensionsAssetsBean) response.getEntity();
        // Assert
        assertThat(bean.getWebItems().get(0), instanceOf(WebItemBean.class));
    }

    @Test
    public void testGetWebItemsWhenNoLocationExistsShouldReturnOk() {
        // Arrange
        resource = new ClientsideExtensionsResource(dynamicWebInterfaceManager);
        // Act
        Response response = resource.getWebItems("nonExistentLocation");
        // Assert
        assertThat(response.getStatus(), is(HttpServletResponse.SC_OK));
    }

    @Test
    public void testGetWebItemsWhenNoLocationExistsShouldReturnEmptyList() {
        // Arrange
        resource = new ClientsideExtensionsResource(dynamicWebInterfaceManager);
        // Act
        Response response = resource.getWebItems("nonExistentLocation");
        ClientsideExtensionsAssetsBean bean = (ClientsideExtensionsAssetsBean) response.getEntity();
        // Assert
        assertThat(bean.getWebItems().size(), is(0));
    }
}
