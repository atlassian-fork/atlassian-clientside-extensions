package com.atlassian.rest.plugins;

import com.atlassian.plugin.web.api.WebItem;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.Mockito.when;

public class WebItemBeanTest {
    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private WebItemBean webItemBean;

    @Mock
    private WebItem webItem;

    @Before
    public void setUp() {
        when(webItem.getCompleteKey()).thenReturn("com.atlassian.test.client-plugins");
        when(webItem.getSection()).thenReturn("reff.test-web-items-location");
        when(webItem.getAccessKey()).thenReturn("test-accesskey");
        when(webItem.getStyleClass()).thenReturn("test-style-class");
        when(webItem.getId()).thenReturn("test-web-item");
        when(webItem.getWeight()).thenReturn(100);
        when(webItem.getUrl()).thenReturn("/mc/bee/url/1");
        when(webItem.getLabel()).thenReturn("A web item dedicated to MC Bee");
        when(webItem.getTitle()).thenReturn("MC Bee");
    }

    @Test
    public void testWebItemBeanConstructor() {
        // Assert
        assertThat(webItemBean, instanceOf(WebItemBean.class));
    }

    @Test
    public void testWebItemBeanHasSetFields() {
        // Arrange
        webItemBean = new WebItemBean(webItem);
        // Assert
        assertThat(webItemBean.getKey(), is("com.atlassian.test.client-plugins"));
        assertThat(webItemBean.getLocation(), is("reff.test-web-items-location"));
        assertThat(webItemBean.getAccessKey(), is("test-accesskey"));
        assertThat(webItemBean.getStyleClass(), is("test-style-class"));
        assertThat(webItemBean.getLinkId(), is("test-web-item"));
        assertThat(webItemBean.getWeight(), is(100));
    }

    @Test
    public void testWebItemBeanHasAttributes() {
        // Arrange
        webItemBean = new WebItemBean(webItem);
        // Assert
        assertThat(webItemBean.getAttributes().get("url"), is("/mc/bee/url/1"));
        assertThat(webItemBean.getAttributes().get("label"), is("A web item dedicated to MC Bee"));
        assertThat(webItemBean.getAttributes().get("title"), is("MC Bee"));
    }

    @Test
    public void testWebItemBeanWithAllowedParamsInAttributes() {
        // Arrange
        Map<String, String> params = new HashMap<String, String>();
        params.put("tooltip", "disabled");
        params.put("mcParams", "mcParamface");
        when(webItem.getParams()).thenReturn(params);
        // Act
        webItemBean = new WebItemBean(webItem);
        // Assert
        assertThat(webItemBean.getAttributes().get("mcParams"), is(nullValue()));
        assertThat(webItemBean.getAttributes().get("tooltip"), is("disabled"));
    }

    @Test
    public void testWebItemsBeanWithParams() {
        // Arrange
        Map<String, String> params = new HashMap<String, String>();
        params.put("tooltip", "disabled");
        params.put("mcParams", "mcParamface");
        when(webItem.getParams()).thenReturn(params);
        // Act
        webItemBean = new WebItemBean(webItem);
        // Assert
        assertThat(webItemBean.getParams().size(), is(2));
        assertThat(webItemBean.getParams().get("mcParams"), is("mcParamface"));
        assertThat(webItemBean.getParams().get("tooltip"), is("disabled"));
    }
}
