package com.atlassian.rest.plugins;

import com.atlassian.ozymandias.SafePluginPointAccess;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.api.WebItem;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.rest.annotation.ResponseType;
import com.google.common.collect.Maps;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.Map;

@Path("client-plugins")
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ClientsideExtensionsResource {
    @ComponentImport
    private DynamicWebInterfaceManager manager;

    @Inject
    public ClientsideExtensionsResource(
        DynamicWebInterfaceManager manager
    ) {
        this.manager = manager;
    }

    @GET
    @Path("/items")
    @ResponseType(ClientsideExtensionsAssetsBean.class)
    public Response getWebItems(@QueryParam("location") String location) {
        ClientsideExtensionsAssetsBean bean = new ClientsideExtensionsAssetsBean(getWebItemsBySection(location));

        return Response.ok(bean).build();
    }

    private Map<String, Object> getContext() {
        final Map<String, Object> context = Maps.newHashMap();
        return context;
    }

    private Iterable<WebItem> getWebItemsBySection(String section) {
        return SafePluginPointAccess.call(() -> manager.getDisplayableWebItems(section, getContext())).getOrElse(Collections.emptyList());
    }
}
