const path = require('path');
const WRMPlugin = require('atlassian-webresource-webpack-plugin');

const OUTPUT_PATH = path.join(__dirname, 'target/classes');

const mode = process.env.NODE_ENV;

const providedDependencies = {};

module.exports = {
    mode,
    entry: {
        runtime: path.resolve(__dirname, 'src/main/frontend/runtime.ts'),
    },
    output: {
        path: OUTPUT_PATH,
        libraryTarget: 'amd',
        library: '@atlassian/clientside-extensions-registry',
        libraryExport: 'default',
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
    },
    module: {
        rules: [{ test: /\.tsx?$/, loader: 'awesome-typescript-loader', options: { errorsAsWarnings: true } }],
    },
    plugins: [
        new WRMPlugin({
            pluginKey: 'com.atlassian.plugins.atlassian-clientside-extensions-runtime',
            xmlDescriptors: path.join(OUTPUT_PATH, 'META-INF', 'plugin-descriptors', 'wr-webpack-bundles.xml'),
            addEntrypointNameAsContext: false,
            webresourceKeyMap: {
                runtime: 'runtime',
            },
            providedDependencies,
        }),
    ],
};
